# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.12
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info >= (2, 7, 0):
    def swig_import_helper():
        import importlib
        pkg = __name__.rpartition('.')[0]
        mname = '.'.join((pkg, '_psnap')).lstrip('.')
        try:
            return importlib.import_module(mname)
        except ImportError:
            return importlib.import_module('_psnap')
    _psnap = swig_import_helper()
    del swig_import_helper
elif _swig_python_version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_psnap', [dirname(__file__)])
        except ImportError:
            import _psnap
            return _psnap
        try:
            _mod = imp.load_module('_psnap', fp, pathname, description)
        finally:
            if fp is not None:
                fp.close()
        return _mod
    _psnap = swig_import_helper()
    del swig_import_helper
else:
    import _psnap
del _swig_python_version_info

try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError("'%s' object has no attribute '%s'" % (class_type.__name__, name))


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except __builtin__.Exception:
    class _object:
        pass
    _newclass = 0

class CameraStruct(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, CameraStruct, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, CameraStruct, name)
    __repr__ = _swig_repr
    def __init__(self):
        this = _psnap.new_CameraStruct()
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _psnap.delete_CameraStruct
    __del__ = lambda self: None
CameraStruct_swigregister = _psnap.CameraStruct_swigregister
CameraStruct_swigregister(CameraStruct)


def getCamera(myCamera):
    return _psnap.getCamera(myCamera)
getCamera = _psnap.getCamera

def getCameraByAddr(myCamera, IpAddress):
    return _psnap.getCameraByAddr(myCamera, IpAddress)
getCameraByAddr = _psnap.getCameraByAddr

def openCamera(myCamera):
    return _psnap.openCamera(myCamera)
openCamera = _psnap.openCamera

def closeCamera(myCamera):
    return _psnap.closeCamera(myCamera)
closeCamera = _psnap.closeCamera

def setupCamera(myCamera, height_value, width_value, x_value, y_value, exposure_time, analog_gain, PixelFormat, black_level):
    return _psnap.setupCamera(myCamera, height_value, width_value, x_value, y_value, exposure_time, analog_gain, PixelFormat, black_level)
setupCamera = _psnap.setupCamera

def startCamera(myCamera, max_frame_rate):
    return _psnap.startCamera(myCamera, max_frame_rate)
startCamera = _psnap.startCamera

def stopCamera(myCamera):
    return _psnap.stopCamera(myCamera)
stopCamera = _psnap.stopCamera

def snapCamera(myCamera):
    return _psnap.snapCamera(myCamera)
snapCamera = _psnap.snapCamera

def getBPMRefImage(myCamera, refImageAddr):
    return _psnap.getBPMRefImage(myCamera, refImageAddr)
getBPMRefImage = _psnap.getBPMRefImage

def findBPMForeground(myCamera, imgAdrForForeground):
    return _psnap.findBPMForeground(myCamera, imgAdrForForeground)
findBPMForeground = _psnap.findBPMForeground

def findBPMBeamParameter(myCamera, imgAdrForBeamParam):
    return _psnap.findBPMBeamParameter(myCamera, imgAdrForBeamParam)
findBPMBeamParameter = _psnap.findBPMBeamParameter

def findBPMScatterMap(myCamera, imgAdrForScatMap):
    return _psnap.findBPMScatterMap(myCamera, imgAdrForScatMap)
findBPMScatterMap = _psnap.findBPMScatterMap

def releaseCameraBuffer(myCamera):
    return _psnap.releaseCameraBuffer(myCamera)
releaseCameraBuffer = _psnap.releaseCameraBuffer

def getFrame(myCamera):
    return _psnap.getFrame(myCamera)
getFrame = _psnap.getFrame

def getFramePtr(myCamera):
    return _psnap.getFramePtr(myCamera)
getFramePtr = _psnap.getFramePtr

def getBeamPosX(myCamera):
    return _psnap.getBeamPosX(myCamera)
getBeamPosX = _psnap.getBeamPosX

def getBeamPosY(myCamera):
    return _psnap.getBeamPosY(myCamera)
getBeamPosY = _psnap.getBeamPosY

def new_longp():
    return _psnap.new_longp()
new_longp = _psnap.new_longp

def copy_longp(value):
    return _psnap.copy_longp(value)
copy_longp = _psnap.copy_longp

def delete_longp(obj):
    return _psnap.delete_longp(obj)
delete_longp = _psnap.delete_longp

def longp_assign(obj, value):
    return _psnap.longp_assign(obj, value)
longp_assign = _psnap.longp_assign

def longp_value(obj):
    return _psnap.longp_value(obj)
longp_value = _psnap.longp_value
# This file is compatible with both classic and new-style classes.


