function I = Ixy(xg, yg)
    I0 = 550; w = 60;
    
    I = I0.*exp(-2*((xg).^2 + (yg).^2)./w.^2);

end