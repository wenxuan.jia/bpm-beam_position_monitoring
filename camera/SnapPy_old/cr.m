function cr(name) % view camera image saved by c code

    %search file with the name specified
    files = dir(['./', name]);
    %search subfolders
    files = [files; dir(['**/', name])];
    
    
    i = 1;
    if(isempty(files))
        error('\n\n No data found with filename ''%s''!!!\n\n', name);

    elseif(length(files) > 1)
        fprintf('\n More than one data found with filename ''%s'':\n', name);
        fprintf('    0: Load all of the following:\n')
        for j = 1:length(files)
            fprintf('    %d: %s\n', j, files(j).name);
        end
        fprintf('\n');
        i = input(' Please choose the one by index: ');

    end
    
    if i == 0
        filename = {};
        for j = 1:length(files)
            fileadr = [files(j).folder, '/', files(j).name];
            filename = [filename; files(j).name];
        end
        
    else
        
        fileadr = [files(i).folder, '/', files(i).name];
        filename = files(i).name;
        
    end
    
    
    f = fopen(fileadr, 'r');
%     data = fscanf(f, '%f', [1 Inf]);
    data = fscanf(f, '%i, %i, %f', [3 Inf]);
    fclose(f);
    
    
    x = data(1,:);
    x = reshape(x, 640, 480);
    figure; imshow(uint8(x'));
    y = data(2,:);
    y = reshape(y, 640, 480);
    figure; imshow(uint8(y'));
    
    img = data(3,:);
    img = reshape(img, 640, 480);
    figure; imshow(uint8(img'));
    
    
    
    
end