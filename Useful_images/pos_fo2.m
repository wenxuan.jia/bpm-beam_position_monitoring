function [posx_fo, posy_fo] = pos_fo2(fgoversg, x0, y0, I0, w) % only has foreground
    fgoversg(isinf(fgoversg)) = NaN;
    
    [xlen, ylen] = size(fgoversg);
    [yg, xg] = meshgrid(1:ylen, 1:xlen); % image indexing is flipped
        
    coeff01 = [];
    coeff10 = [];
    delta = 1:1:3;
    
    for d = delta
        
        ig01 = I0.*exp(-2*((xg-x0).^2 + (yg-y0+d).^2)./w.^2) - I0.*exp(-2*((xg-x0).^2 + (yg-y0).^2)./w.^2);
        ig10 = I0.*exp(-2*((xg-x0+d).^2 + (yg-y0).^2)./w.^2) - I0.*exp(-2*((xg-x0).^2 + (yg-y0).^2)./w.^2);
        coeff01 = [coeff01; nansum(nansum(ig01.*fgoversg))];
        coeff10 = [coeff10; nansum(nansum(ig10.*fgoversg))];
        
        
    end
    
%     dy = -3:0.01:3;
%     err = a(1)*(exp((-2*dy*deltay(2)-deltay(2)^2)/w^2)-1) - a(2)*(exp((-2*dy*deltay(1)-deltay(1)^2)/w^2)-1);
%     figure; plot(dy, err);
    
    deltay = []; deltax = [];
    syms x;
    
    for i = 1:length(delta)-1
        for j = i+1:length(delta)
            dy = eval(vpasolve(coeff01(i)*(exp((-2*x*delta(j)-delta(j)^2)/w^2)-1) - coeff01(j)*(exp((-2*x*delta(i)-delta(i)^2)/w^2)-1) == 0, x));
            deltay = [deltay, dy];

            dx = eval(vpasolve(coeff10(i)*(exp((-2*x*delta(j)-delta(j)^2)/w^2)-1) - coeff10(j)*(exp((-2*x*delta(i)-delta(i)^2)/w^2)-1) == 0, x));
            deltax = [deltax, dx];
        end
    end
    
    
    posx_fo = x0 + mean(deltax);
    posy_fo = y0 + mean(deltay);
end
