function [px, py, sg, I0, w] = find1stScat(fg, priorix, prioriy) 
    
    [xlen, ylen] = size(fg);
    [yg, xg] = meshgrid(1:ylen, 1:xlen); % image indexing is flipped

    % find the centroid of the first image
    
    posx = nansum(nansum(fg.*xg))/nansum(nansum(fg)); 
    posy = nansum(nansum(fg.*yg))/nansum(nansum(fg));
    
    % beam center pos from dither
    
    
    
    % calibrate beam intensity and radius
    
    % brute force
%     deltax = 1:.5:4;
%     deltay = -1:.5:2;
% 
%     [eyg, ~] = meshgrid(deltay, deltax);
%     err = zeros(size(eyg));
% 
% 
%     for i = 1:length(deltax)
%         for j = 1:length(deltay)
%             err(i,j) = f1e_fun(posx+deltax(i), posy+deltay(j), img);
% 
%         end
%     end
%     
%     figure; surf(deltay, deltax, err); shading interp; view([0 90]); colormap('jet');
%     xlabel('Y error'); ylabel('X error')
%     return



    if ~exist('priorix', 'var')
        stepsizex = 0.005;
    else
        stepsizex = 0;
        posx = priorix;
    end
    
    if ~exist('priorix', 'var')
        stepsizey = 0.005;
    else
        stepsizey = 0;
        posy = prioriy;
    end
    
    
    derrd = [NaN, NaN];
    
    posstep = 0.1;
    
    damping = 0;  % it's in [0,1]. 0 is no damping.
    
    tol = 0.1;
    
    i = 0;
    accumu = 0;
    stepmoved = Inf;
    
%     figure 
%     h = animatedline; 
%     axis([x0-3, x0+3, y0-3, y0+3]) 
    
    
    while stepsizex + stepsizey > 0 % pixel unit
        if i > 1
            posx(i+1) = posx(i) - stepsizex*( (1)*derrd(i,1) + damping*derrd(i-1,1) );
            posy(i+1) = posy(i) - stepsizey*( (1)*derrd(i,2) + damping*derrd(i-1,2) );
        elseif i == 1
            posx(i+1) = posx(i) - stepsizex*derrd(i,1);
            posy(i+1) = posy(i) - stepsizey*derrd(i,2);
        end
        i = i+1; accumu = accumu + 1;
        derrd(i,1) = f1e_fun(posx(i)+posstep, posy(i), fg) - f1e_fun(posx(i), posy(i), fg);
        derrd(i,2) = f1e_fun(posx(i), posy(i)+posstep, fg) - f1e_fun(posx(i), posy(i), fg);
        derrd(i,:) = derrd(i,:)/posstep;
        
        if i == 1
            stepmovedx = stepsizex*derrd(i,1);
            stepmovedy = stepsizey*derrd(i,2);
        else
            stepmovedx = posx(i) - posx(i-1);
            stepmovedy = posy(i) - posy(i-1);
        end
        
        if abs(stepmovedx) < tol
            stepsizex = 0;
        end
        
        if abs(stepmovedy) < tol
            stepsizey = 0;
        end
        
        
        if accumu > 20
            damping = damping + 0.1;
            stepsizex = stepsizex * 0.7;
            stepsizey = stepsizey * 0.7;
            
            accumu = 0;
        end
        
%         addpoints(h,xgd(i),ygd(i)) 
%         drawnow limitrate nocallbacks
        
    end
    
    
    
    [~, I0, w] = f1e_fun(posx(end), posy(end), fg, true);
    
    % scatter/reflectivity map
    
    sg = getScatterMap(fg, posx(end), posy(end), I0, w); 
    
    px= posx(end); py = posy(end);
    

end



