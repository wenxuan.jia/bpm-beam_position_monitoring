#!/usr/bin/env python

import sys
import os
import getopt
import ConfigParser
import shlex, subprocess
from datetime import datetime
import time


def usage():
    print "-h, --help          Displays this message"
    print "-c, --config        Uses the settings in the config file listed"


#Default
host = "127.0.0.1"
port = "5000"


#Parse command line options
try:
   opts,args = getopt.getopt(sys.argv[1:],"hc:",["help","config="])
except getopt.GetoptError:
   usage()
   sys.exit(2)

#if len(opts) == 0:
#    usage()
#    sys.exit()

for o, a in opts:

    if o in ("-h", "--help"):
        usage()
        sys.exit()
    elif o in ("-c", "--config"):
        configFile = str(a)
    else:
        usage()
        sys.exit()


#Create config parser and grab necessary configuration information
config = ConfigParser.ConfigParser()
configAddress = "/ligo/cds/llo/l1/camera/"
configFile = [configAddress + "L1-CAM-ITMX.ini", configAddress + "L1-CAM-ETMX.ini", configAddress + "L1-CAM-ITMY.ini", configAddress + "L1-CAM-ETMY.ini"]

someName = raw_input("\nEnter some names:  ").strip()

dur = raw_input("\nEnter duration in minutes (0 for infinity):  ")
dur = float(dur)


rep = raw_input("\nEnter number of times to run (0 is no repeat):  ")
rep = int(rep)

if rep != 0:
	interval = raw_input("\nEnter how long it stops before next run in minutes:  ")
	interval = int(interval)

else:
	interval = 0


for i in range(rep+1):
	args = ["" for x in range(4)]

	for m in range(4): # loop through 4 test masses
		config.read(configFile[m])
		port = config.get('No Reload Camera Settings','Multicast Port')
		multicastGroup = config.get('No Reload Camera Settings','Multicast Group')
		cameraName = config.get('Camera Settings', 'Camera Name')
		
		now = datetime.utcnow()
		utctime = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + "-" + str(now.hour) + "-" + str(now.minute) + "-" + str(now.second)
		

		user_file = "/data/wenxuan.jia/BPM/Useful_images/testing/" + utctime + "_" + cameraName + "_" + someName + ".avi"
		

	    
		path, file_name = os.path.split(user_file)
	
		commandString = ''.join(["gst-launch udpsrc multicast-group=",str(multicastGroup)," port=",str(port), " ! application/x-rtp, payload=127, latency=0 ! rtph264depay ! ffdec_h264 ! ffmpegcolorspace ! ffenc_mpeg4, latency=0, framerate=25 ! avimux ! filesink location=",str(user_file)])

		args[m] = shlex.split(commandString)

		

	# The video captured have different frame counts...	
	proc0 = subprocess.Popen(args[0])
	proc1 = subprocess.Popen(args[1])
	proc2 = subprocess.Popen(args[2])
	proc3 = subprocess.Popen(args[3])
	time.sleep(dur*60)

	proc0.kill()
	proc1.kill()
	proc2.kill()
	proc3.kill()

	time.sleep(interval*60)
	print "Captures done: " + str(i+1) + "\n" 


	



       




