#!/usr/bin/env python

#pcamerasrc.py: implements a basler camera source element in python
# using psnap.py c++ bindings (basically basler API bindings)

#Joseph Betzwieser 2014/01/30

#Needed imports
import sys
import gobject
import pygst
pygst.require('0.10')
import gst
import psnap
import numpy
import time
import logging
import logging.handlers
import os
import ctypes
import epics
from epics import PV

MAX_CONNECTION_RETRIES = 3 

class SingleCopyFilter(logging.Filter):

    def __init__(self):
        self.timer = time.time()
        self.messageBuffer = []
        self.messageCount = 0

    def filter(self, record):
        #This sets the time between sending messages
        if time.time() - self.timer > 1.0:
            self.messageBuffer = []
            self.timer = time.time()
            record.msg = 'Duplicate messages in the previous second: ' + str(self.messageCount) + ' ... ' + str(record.getMessage())
            self.messageCount = 0
        elif record.getMessage() in self.messageBuffer:
            self.messageCount = self.messageCount + 1
            return 0
        self.messageBuffer.append(record.getMessage())
        return 1

class snapCameraTimeoutError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class snapCameraNoGrabError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class getFrameNoImageError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class PCameraSource(gst.BaseSrc):
    __gsttemplates__ = (
        gst.PadTemplate("src",
                        gst.PAD_SRC,
                        gst.PAD_ALWAYS,
                        gst.caps_new_any()),
        )



    #Various flags to indicate state of readiness
    #cameraRunningFlag = False
    #parameterChangeFlag = True
    def __init__(self,name):
        self.__gobject_init__()
        self.myCamera = psnap.CameraStruct()
        #default parameters
        self.set_name(name)
        self.exposure = 10000
        self.height = 480
        self.width = 640
        self.Xpos = 0 #corresponds to width, UL corner, measured from left
        self.Ypos = 0 #corresponds to height, UL corner, measured from top
        self.frameType = 'Mono8'
        self.gain = 100
        self.cameraIP = None
        self.packetSize = 9000
        self.cameraRunningFlag = False
        self.parameterChangeFlag = True
        self.connectedToCameraFlag = False
        self.numpyArray = numpy.array([0])
        self.handOff = [0]
        self.colorDepth = '8'
        self.colorBPP = '8'
        self.startTime = 0
        self.frameCount = 0
        self.maxFrameRate = 25
        self.time = time.time()
        self.set_live(True)
        self.setupBPM = False
        self.comXChannelPV = PV('L1:CAM-ETMY_WX',None,'native',None,None,1)
        self.comYChannelPV = PV('L1:CAM-ETMY_WY',None,'native',None,None,1)
        self.ISCGRDChannelPV = PV('L1:GRD-ISC_LOCK_STATE_N',None,'native',None,None,1)
	#self.comXChannelPV = PV('L1:ASC-ADS_CAM_YAW14_INPUT',None,'native',None,None,1)
        #self.comYChannelPV = PV('L1:ASC-ADS_CAM_PIT14_INPUT',None,'native',None,None,1)
        #self.comXChannelPV = PV('L1:CAM-PRM_LAV_X',None,'native',None,None,1)
        #self.comYChannelPV = PV('L1:CAM-PRM_LAV_Y',None,'native',None,None,1)

        #Default logging setup
        self.logFileName = None
        self.logFileLevel = str(logging.INFO)
        self.logger = logging.getLogger('gstreamer')
        self.logger.setLevel(int(self.logFileLevel))
        self.handler = logging.StreamHandler()
        self.handler.setLevel(int(self.logFileLevel))
        formatter = logging.Formatter('%(asctime)s UTC - %(levelname)s - %(message)s')
        formatter.converter = time.gmtime
        self.handler.setFormatter(formatter)
        self.logger.addHandler(self.handler)

    #Handles the setting of camera parameters
    def set_property(self, name, value):
        self.parameterChangeFlag = True
        if name == 'exposure':
            self.exposure = value
        elif name == 'height':
            self.height = value
        elif name == 'width':
            self.width = value
        elif name == 'IP':
            self.cameraIP = value
        elif name == 'X':
            self.Xpos = value
        elif name == 'Y':
            self.Ypos = value
        elif name == 'frameType':
            self.frameType = value
            if self.frameType == 'Mono8':
                self.colorBPP = '8'
                self.colorDepth = '8'
            elif self.frameType == 'Mono12':
                self.colorBPP = '16'
                self.colorDepth = '16'
            elif self.frameType == 'RGB8':
                self.colorBPP = '24'
                self.colorDepth = '24'
        elif name == 'gain':
            self.gain = value
        elif name == 'blackLevel':
            self.blackLevel = value
        elif name == 'packetSize':
            self.PacketSize = value
        elif name == 'reset':
            self.parameterChangeFlag = True
        elif name == 'NumpyArrayReference':
            self.numpyArray = value
        elif name == 'NumpyArrayHandOff':
            self.handOff = value
        elif name == 'maxFrameRate':
            self.maxFrameRate = value
        elif name == 'logFileName':
            if value != None:
                self.logFileName = value
                self.logger = logging.getLogger('gstreamer')
                self.logger.setLevel(int(self.logFileLevel))
                self.logger.removeHandler(self.handler)
                self.handler = logging.handlers.TimedRotatingFileHandler(self.logFileName,when='W6',delay=True)
                self.handler.setLevel(int(self.logFileLevel))
                formatter = logging.Formatter('%(asctime)s UTC - %(levelname)s - %(message)s')
                formatter.converter = time.gmtime
                self.handler.setFormatter(formatter)
                self.logger.addHandler(self.handler)
                self.logger.addFilter(SingleCopyFilter())

            else:
                self.logFileLevel = str(logging.INFO)
                self.logger = logging.getLogger('gstreamer')
                self.logger.setLevel(int(self.logFileLevel))
                self.handler = logging.StreamHandler()
                self.handler.setLevel(int(self.logFileLevel))
                formatter = logging.Formatter('%(asctime)s UTC - %(levelname)s - %(message)s')
                formatter.converter = time.gmtime
                self.handler.setFormatter(formatter)
                self.logger.addHandler(self.handler)
                self.logger.addFilter(SingleCopyFilter())



    def turn_camera_off(self):
        psnap.stopCamera(self.myCamera)
        psnap.closeCamera(self.myCamera)
        self.cameraRunningFlag = False

    def connect_to_camera(self):
        if (self.cameraIP != None):
            retries = 1
            while retries <= MAX_CONNECTION_RETRIES:
                if psnap.getCameraByAddr(self.myCamera,self.cameraIP):
                    self.connectedToCameraFlag = True
                    return True
                else:
                    self.logger.error("Failed to setup camera with IP = " + str(self.cameraIP) +", retry attempt " + str(retries) + " (max " + str(MAX_CONNECTION_RETRIES) + ")")
                    self.handler.flush()
                    time.sleep(3)
                    retries = retries + 1
        else:
            self.logger.error("No IP address defined, failed to setup a camera")
            self.handler.flush()
            return False

    def setup_start_camera(self):
        if psnap.openCamera(self.myCamera) == 0:
            self.logger.info("Opened camera")
            self.handler.flush()
            if psnap.setupCamera(self.myCamera, self.height,
                                self.width, self.Xpos, self.Ypos,
                                self.exposure,self.gain,self.frameType,self.blackLevel):
                self.logger.info("setupCamera done")
                sys.stdout.flush()
                if psnap.startCamera(self.myCamera,self.maxFrameRate):
                    self.logger.info("startCamera done")
                    sys.stdout.flush()
                    self.cameraRunningFlag = True
                    self.parameterChangeFlag = False
                    return True
        self.logger.error("Failed to start camera")
        self.handler.flush()
        return False

    def writeBPMRefImage(self, refImgAddr):
	psnap.getBPMRefImage(self.myCamera, refImgAddr)
	return True

    def setup_beam_monitor(self, imgaddress):
        psnap.findBPMForeground(self.myCamera, imgaddress)
        psnap.findBPMBeamParameter(self.myCamera, imgaddress)
        psnap.findBPMScatterMap(self.myCamera, imgaddress)
        return True
        

    def take_snap(self):
        snapCameraReturn = psnap.snapCamera(self.myCamera)
        if snapCameraReturn == 0:
            imageFrame = psnap.getFrame(self.myCamera)
            #Did it work? If yes, make a gst.Buffer and return it
            if imageFrame:
  	  	if (self.handOff[0] == 1):
                    if self.frameType == 'Mono8':
                        self.numpyArray[0] = numpy.frombuffer(imageFrame,dtype=numpy.uint8).copy()
                        self.numpyArray[0].shape = [self.height,self.width]
                    if self.frameType == 'Mono12':
                        self.numpyArray[0] = numpy.frombuffer(imageFrame,dtype=numpy.uint16).copy()
                        self.numpyArray[0].shape = [self.height,self.width]
                    if self.frameType == 'RGB8':
                        self.numpyArray[0] = numpy.frombuffer(imageFrame,dtype=numpy.unit8).copy()
                        self.numpyArray[0].shape = [self.height,self.width*3]
                    self.handOff[0] = 0
                

		#temp = self.numpyArray[0];
		#print(temp)
		#for i in range(5):
		#    print('img[', 480*640-1-i, '] = ', temp[480*640-1-i])

		if self.frameType == 'Mono12':
                    imageFrame = numpy.getbuffer(numpy.frombuffer(imageFrame,dtype=numpy.uint16)*16)

                imageBuffer = gst.Buffer(imageFrame)
                imageBuffer.duration = int(self.exposure*1000) #convert to nanoseconds from microseconds
                self.startTime = self.startTime +1
                imageBuffer.timestamp = self.startTime
                if self.frameType == 'Mono12':
                    caps = gst.caps_from_string(''.join(["video/x-raw-gray, height=",str(self.height),
                                                     ", width=",str(self.width),", bpp=",str(self.colorBPP),
                                                     ",depth=",str(self.colorDepth),",endianness=1234",", framerate=25/1"]))
                elif self.frameType == 'Mono8':
                    caps = gst.caps_from_string(''.join(["video/x-raw-gray, height=",str(self.height),
                                                     ", width=",str(self.width),", bpp=",str(self.colorBPP),
                                                     ",depth=",str(self.colorDepth),", framerate=25/1"]))
                elif self.frameType == 'RGB8':
                    caps = gst.caps_from_string(''.join(["video/x-raw-rgb, height=",str(self.height),
                                                    ", width=",str(self.width),", bpp=",str(self.colorBPP),
                                                    ",depth=",str(self.colorDepth),", framerate=25/1"]))
                imageBuffer.set_caps(caps)
                self.frameCount = self.frameCount + 1
                if time.time() - self.time > 1.0:
                   self.logger.debug(str(self.frameCount))
                   self.handler.flush()
                   self.frameCount = 0
                   self.time = time.time()
		
		try:	
		   lockState = self.ISCGRDChannelPV.get()
		#print("ISC state = ", lockState)
                except:
		   print("ISC LOCK STATE is not available")
		   lockState = 0;


		if lockState >= 0:
		    if not self.setupBPM:
            	        print("Setting up BPM")
			BPMRefImgAddr = '/usr/local/home/controls/SnapPy/refImage/07112022.txt'
			try:
			   #self.writeBPMRefImage(BPMRefImgAddr)
			   #print("Writing reference image for BPM")
	    	           self.setup_beam_monitor(BPMRefImgAddr)
            	           print("BPM set up done")
			except:
			   print("BPM set up failed!")
		    	
			self.setupBPM = True


        	    else:
			try:
		           posx = psnap.getBeamPosX(self.myCamera)
		           #print('X = ', posx)
		           posy = psnap.getBeamPosY(self.myCamera)
		           #print('Y = ', posy)
		           # push posx and posy to the epics channel
                           self.comXChannelPV.put(float(posx))
                           self.comYChannelPV.put(float(posy))
			except:
			   print("getBeamPos failed!")
		
                psnap.releaseCameraBuffer(self.myCamera) 
                 
	        return imageBuffer
            else:
                psnap.releaseCameraBuffer(self.myCamera)
                self.logger.error("imageFrame does not exist")
                self.handler.flush()
                raise getFrameNoImageError('getFrame failed: no image')
        elif snapCameraReturn == 1:
            #psnap.releaseCameraBuffer(self.myCamera)
            self.logger.error("psnap.snapCamera failed: could not grab frame")
            self.handler.flush()
            raise snapCameraNoGrabError('psnap.snapCamera failed: could not grab frame')
        elif snapCameraReturn  == 2:
            self.logger.error("psnap.snapCamera failed: timeout")
            self.handler.flush()
            raise snapCameraTimeoutError("psnap.snapCamera failed: timeout")

        #FIXME: Need some better error handling - i.e. how to send 
        #approriate messages other than unexpected
    def do_create(self, offset, size):
       #Are we running and need to change something? If yes, stop
        if self.cameraRunningFlag and self.parameterChangeFlag:
            self.turn_camera_off()
        #Do we have things to change?
        if self.parameterChangeFlag:
            if not self.connectedToCameraFlag:
                #Is there an actual camera IP setting?
                if (self.cameraIP == None): 
                    self.logger.error("Failed to find a camera, no IP to use")
                    self.handler.flush()
                    return gst.FLOW_UNEXPECTED, None
                #Grab the camera with the now set UID
                #Us IP if available, otherwise go by UID
                if not self.connect_to_camera():
                    sys.exit(1)
                    return gst.FLOW_UNEXPECTED, None
        #If simply not running, try to start running    
        if self.cameraRunningFlag == False:
            if self.connectedToCameraFlag == False:
                #If unconnected to camera, try to connect
                if not self.connect_to_camera():
                    sys.exit(1)
                    return gst.FLOW_UNEXPECTED, None
            #Start the camera with current parameters
            if not self.setup_start_camera():
                return gst.FLOW_UNEXPECTED, None
        #We're running and have everything updated 
        #Just take a picture
        try:
	    #print("try taking a snap")
            imageBuffer = self.take_snap()
#        if imageBuffer != None and imageBuffer != "Timeout":
            return gst.FLOW_OK, imageBuffer
        except (snapCameraTimeoutError, snapCameraNoGrabError,getFrameNoImageError):
            self.logger.error("Attempting to stop camera")
            self.handler.flush()
            self.turn_camera_off()
            self.logger.error("Attempting to start camera")
            self.handler.flush()
            setupReturn = self.setup_start_camera()
            self.logger.error("setupReturn = " + str(setupReturn))
            self.handler.flush()
            while True:
                if setupReturn:
                    #Now try up to 10 more times to get the image again
                    for tries in range(1,10):
                        self.logger.error(''.join(["Failed to get frame - try number: ", str(tries)]))
                        self.handler.flush()
                        try:
                            imageBuffer = self.take_snap()
                            return gst.FLOW_OK, imageBuffer 
                        except (snapCameraTimeoutError, snapCameraNoGrabError,getFrameNoImageError):
                            pass
                    self.logger.error("Attempting to stop camera")
                    self.handler.flush()
                    self.turn_camera_off()
                else:
                    self.logger.error("setup_start_camera failed")
                    self.handler.flush()
                time.sleep(1)
                self.logger.error("Attempting to start camera")
                self.handler.flush()
                setupReturn = self.setup_start_camera()

gobject.type_register(PCameraSource)
