function [fgft, bgft] = ftDetector(refimg_s, noiseThreshold, clusterDist, clusterMin) % img has saturation
    
    
    
    % hierarchical agglomerative clustering
    
    
    
    %%%%%%%%%%%%%%%%%%%%%
%     refimg = rawimg{1}; % reference image
%     noiseThreshold = 50; % from 1 to 255
%     clusterDist = 10; % pixel
%     clusterMin = 20; % minimal pixels to form a background cluster
    
    %%%%%%%%%%%%%%%%%%%%%
    
    
    
    [xlen, ylen] = size(refimg_s);
    [yg, xg] = meshgrid(1:ylen, 1:xlen);
    
    pos_index = refimg_s >= noiseThreshold;
    
    pos_x = xg(pos_index);
    pos_y = yg(pos_index);
    intensity = refimg_s(pos_index);
    
    
    group = clusterdata([pos_x, pos_y], 'Criterion', 'Distance', 'cutoff', clusterDist);
%     group = group0;
    
    grouptot = max(group);
    
    
    [N, edge] = histcounts(group, 1:grouptot+1); 
    edge(end) = [];
    
    edge = edge + grouptot;
    group = group + grouptot;
    
    [~, tmp] = sort(N); 
    edge = edge(tmp); % group index number sorted by frequency
    N = N(tmp); % group size sorted by frequency (ascending)
    
    
    newgroupindex = 1; power = [];
    centroid_x = []; centroid_y = [];
    % remove cluster smaller than clusterMin
    % sort group number by member size
    for j = length(N):-1:1
        
        index = group == edge(j);
        
        if N(j) > clusterMin
            group(index) = newgroupindex;
            
            if newgroupindex == 1 % combine all background features
            newgroupindex = newgroupindex + 1;
            end
            power = [power; sum(intensity(index))];
            centroid_x = [centroid_x; mean(pos_x(index))];
            centroid_y = [centroid_y; mean(pos_y(index))];
            
        else
            
            group(index) = [];
            pos_x(index) = [];
            pos_y(index) = [];
            intensity(index) = [];
            
        end
        
    end
    
    if power(1) ~= max(power)
        error('Camera exposure too high! \n\n')
    end
    
    
    
%     figure; scatter(pos_y, -pos_x, 1, group); hold on
%     
%     xlim([0 640])
%     ylim([-480 0])
%     colormap(gca, 'lines');
    
    
    
    
    
    % set up feature regions
    fgft = [];
    bgft = [];
    
    
    
    for i = 1:max(group)
%     for i = 1
        id = group == i;
        px = pos_x(id);
        py = pos_y(id);
        
        shrinkf = 0; % 0 for most convex polygon
        bd = boundary(px, py, shrinkf);
        
        bdx = px(bd);
        bdy = py(bd);
        
        extlen = sqrt((bdx - centroid_x(i)).^2 + (bdy - centroid_y(i)).^2);
        
        bdx = bdx + clusterMin*(bdx - centroid_x(i))./extlen;
        bdy = bdy + clusterMin*(bdy - centroid_y(i))./extlen;
        
%         plot(bdy, -bdx, 'k-')
        
        if i == 1
            fgft.polyid = inpolygon(xg, yg, bdx, bdy);
            fgft.cx = centroid_x(i);
            fgft.cy = centroid_y(i);
            fgft.pwr = power(i);
            
        else
            % the background features are not combined into one
            bgft(i-1).polyid = inpolygon(xg, yg, bdx, bdy);
            
            % exclude the foreground features
            bgft(i-1).polyid = bgft(i-1).polyid & ~fgft.polyid;
            
            
            bgft(i-1).ref = refimg_s(id); % the features in polygon
            bgft(i-1).posx = px;
            bgft(i-1).posy = py;
            bgft(i-1).cx = centroid_x(i-1);
            bgft(i-1).cy = centroid_y(i-1);
            bgft(i-1).pwr = power(i-1);
            
        end
        
    end
        
%     clearvars -except fgft bgft
    
    
    
    
    
    
%     figure; scatter(pos_y(group == mode(group)), -pos_x(group == mode(group)), 1);
%     xlim([0 640])
%     ylim([-480 0])
    
    
    
    
    % primitive version
    %{
    imgnum = 2;
    fg = {};
    bg = {};
    
    for imgnum = 1:length(rawimg)
    
    m = rawimg{imgnum};
    
    
    top = 190; 
    bot = 350; 
    lef = 190; 
    rig = 350;
    
%     m(top, lef:rig) = 255;
%     m(bot, lef:rig) = 255;
%     m(top:bot, lef) = 255;
%     m(top:bot, rig) = 255;
    
    
%     imshow(m); 
%     rectangle('position', [left rig-lef lef rig], 'edgecolor', 'r')
    
    ftr = m*0;
    ftr(top:bot, lef:rig) = 1;
    
    fg = [fg; m.*ftr];
    bg = [bg; m.*uint8(~ftr)];
    
%     figure; imshow(bg); 
%     figure; imshow(fg);
%     
%     figure; surf(fg); shading interp; colormap('turbo'); view([0 90])
    end
    
    [x,y] = size(fg{1});
    [yg, xg] = meshgrid(1:y, 1:x); % image indexing is flipped
    %}
    
    
    
    
    return

end