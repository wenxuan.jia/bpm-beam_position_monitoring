#pragma once

#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;


// Point in the xy-plane
struct PointType {
    double x;
    double y;
    int id;
};


// Sort criterion: points are sorted with respect to their x-coordinate.
//                 If two points have the same x-coordinate then we compare
//                 their y-coordinates
bool sortPoints(const PointType& lhs, const PointType& rhs);


// Check if three points make a right turn using cross product
bool right_turn(const PointType& P1, const PointType& P2, const PointType& P3);


vector<int> convexHull(double* xlist, double* ylist, int listnum);