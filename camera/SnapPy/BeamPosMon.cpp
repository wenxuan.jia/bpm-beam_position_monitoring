/*
 * BeamPosMon.cpp
 *
 *
 * Prep: get scatter map and origin of beam position. 
 * 
 * 
 * Data array diagram
 *
 *
 *                       [yy, yy, yy, yy                 yy, yy, yy] = sgid, only non-saturated < 255
 *                        ↑   ↑   ↑    ↑                  ↑  ↑   ↑
 *                       [xx, xx, xx, ... ... ... ... ..., ..., xx] = fgid, only foreground > clusterNoiseThreshold
 *                         ↑ ... ... ... ... ... ... ... ... ...↑
 *       id = [0, 1, 2, 3, ... ... ... ... ... ... ... ... ... ... ... .. , 480*640-1]
 * rawImage = [2, 1, ... , 105, 32, ... , 255, 255, ... , 94, 112, ..., 4, 1, 3, 2, 4]
 *       xg = [0, 1, ... , 639, 0, 1, ... , 639, ... ,   0, ... , 639,   0, ... , 639]
 *       yg = [0, 0, ... ,   0, 1, 1, ... ,   1, ... , 478, ... , 478, 479, ... , 479]
 * 
 *
 *
 * Wenxuan Jia, June 2022
 *
 */

#include "BeamPosMon.h"

BeamPosMon::BeamPosMon()
{
    imgr = 480;
    imgc = 640;
    saturation = 255;

    xg = new int[imgr*imgc];
    yg = new int[imgc*imgc];
    image = new double[imgr * imgc];


    //// MATLAB convention of defining x and y coordinates. We asssume images double* have same mapping.
    //for (int j = 0; j < imgc; j++) {
    //    for (int i = 0; i < imgr; i++){
    //        xg[j * imgr + i] = i;
    //        yg[j * imgr + i] = j;
    //    } 
    //}

    // Camera convention of defining x and y coordinates.
    for (int j = 0; j < imgr; j++) {
        for (int i = 0; i < imgc; i++) {
            xg[j * imgc + i] = i;
            yg[j * imgc + i] = j;
        }
    }


    // initialize pointers
    //clusterLabel = NULL;
    

    scatMapFileName = "scatMap";
    refImgFileNamePrefix = "";

    isImgFile1Col = false;

    saturation = 255;
    clusterDist = 10;
    clusterNoiseThreshold = 50;
    foregroundClusterPadding = 20;
    saveEachScatterMap = true;

    initialGuessI0 = 400;
    initialGuessW = 60;
    LMgradientDelta = 0.01;
    lmstat_verbose = 1;
    lmstat_max_it = 10000;
    lmstat_init_lambda = 0.0001;
    lmstat_up_factor = 10;
    lmstat_down_factor = 10;
    lmstat_target_derr = 1e-12;

    posGrid = { -5, 0, 5 };
}

//BeamPosMon::~BeamPosMon()
//{
//    delete[] xg, yg;
//    delete[] clusterXList, clusterYList;
//    delete[] fgid;
//    delete[] fgx, fgy;
//    delete[] sgid;
//    delete[] sgx, sgy;
//    delete[] sg;
//
//}

double BeamPosMon::I_00(double x, double y, double I0, double w)
{
    return I0 * exp(-2 * (x * x + y * y) / (w * w));
}

double BeamPosMon::I_10(double x, double y, double I0, double w, double deltax)
{
    return I0 * exp(-2 * ((x + deltax) * (x + deltax) + y * y) / (w * w)) - I0 * exp(-2 * ((x - deltax) * (x - deltax) + y * y) / (w * w));
}

double BeamPosMon::I_01(double x, double y, double I0, double w, double deltay)
{
    return I0 * exp(-2 * (x * x + (y + deltay) * (y + deltay)) / (w * w)) - I0 * exp(-2 * (x * x + (y - deltay) * (y - deltay)) / (w * w));
}

double BeamPosMon::interp1(vector<double> x, vector<double> y, double xq)
{
    if (x.size() != y.size()) {
        fprintf(stderr, "x and y have unequal size\n");
        return 0.0;
    }

    int N = int(x.size());
    int l;

    int i = 0;
    while (xq > x[i] && i < N-1) {
        i++;
    }

    if (i < 2) {
        l = 0; 
    }
    else if (i > N-2) {
        l = N - 2;
    }
    else {
        l = i - 1;
    }

    return y[l] + (y[l + 1] - y[l]) / (x[l + 1] - x[l]) * (xq - x[l]);
}

bool BeamPosMon::loadImageFromTxt(double* img, string opt_infile)
{
    FILE* f = fopen(opt_infile.c_str(), "r");
    if (!f) {
        fprintf(stderr, "Cannot open '%s'\n", opt_infile.c_str());
        return false;
    }
    double imgdata;
    int tempx, tempy;
    for (int i = 0; i < imgr * imgc; i++) {
        if (isImgFile1Col) {
            fscanf(f, "%lf\n", &imgdata);
        }
        else
        {
            fscanf(f, "%i, %i, %lf\n", &tempx, &tempy, &imgdata);
        }
        
        img[i] = imgdata;

    }
    fclose(f);

    return true;
}

bool BeamPosMon::loadScatterMap(string imgAddr)
{
    string opt_infile = imgAddr + scatMapFileName + ".txt";
    FILE* f = fopen(opt_infile.c_str(), "r");
    if (!f) {
        fprintf(stderr, "Cannot open '%s'\n", opt_infile.c_str());
        return false;
    }
    double sgdata;
    int tempsx, tempsy, tempid;
    vector<int> sgxvec, sgyvec, sgidvec; // vector version
    vector<double> sgvec; // vector version of sg


    int k;
    while (!feof(f)) {
        k = fscanf(f, "%i, %i, %i, %lf\n", &tempid, &tempsx, &tempsy, &sgdata);
        sgidvec.push_back(tempid);
        sgxvec.push_back(tempsx);
        sgyvec.push_back(tempsy);
        sgvec.push_back(sgdata);

    }
    fclose(f);

    delete[] sgid, sgx, sgy, sg;
    sgn = int(sgvec.size());
    sgid = new int[sgn];
    sgx = new int[sgn];
    sgy = new int[sgn];
    sg = new double[sgn];

    for (int i = 0; i < sgn; i++) {
        sgid[i] = sgidvec[i];
        sgx[i] = sgxvec[i];
        sgy[i] = sgyvec[i];
        sg[i] = sgvec[i];
    }

    cout << "BPM: Scatter map " << opt_infile << " loaded. Total " << sgn << " points in the map grid. " << endl;


    return true;
}

void BeamPosMon::saveImageToTxt(double* img, string opt_outfile)
{
    
    FILE* f = fopen(opt_outfile.c_str(), "w");
    if (!f) {
        fprintf(stderr, "Cannot open '%s'\n", opt_outfile.c_str());
        return;
    }
    for (int i = 0; i < imgr * imgc; i++) {
        //fprintf(f, "%3.2f\n", img[i]);
        fprintf(f, "%i, %i, %lf\n", xg[i], yg[i], img[i]);
    }
    fclose(f);
    cout << "BPM: Writing image to " << opt_outfile << endl;
    return;
}

void BeamPosMon::saveScatterMap(string opt_outfile)
{
    FILE* f = fopen(opt_outfile.c_str(), "w");
    if (!f) {
        fprintf(stderr, "Cannot open '%s'\n", opt_outfile.c_str());
        return;
    }
    for (int i = 0; i < sgn; i++) {
        //fprintf(f, "%3.2f\n", img[i]);
        fprintf(f, "%i, %i, %i, %lf\n", sgid[i], sgx[i], sgy[i], sg[i]);
    }
    fclose(f);
    cout << "BPM: Writing scatter map to " << opt_outfile << endl;
    return;
}


bool BeamPosMon::clusterData(string opt_infile)
{
    cout << "BPM: Clustering data from " << opt_infile << endl;

    // 2D point or vector
    class Point_ {
    public:
        double x;
        double y;
        Point_(double xx = 0.0, double yy = 0.0) { x = xx; y = yy; }
        Point_(const Point_& p) { x = p.x; y = p.y; }
        double norm() { return(sqrt(x * x + y * y)); }
    };

    // line segment
    class Segment {
    public:
        Point_ p1;
        Point_ p2;
        Point_ dir;
        Segment(const Point_ p, const Point_ q) { p1 = p, p2 = q; dir = direction(); }
        Segment(const Segment& s) { p1 = s.p1, p2 = s.p2; dir = direction(); }
    private:
        Point_ direction() {
            Point_ d(p2.x - p1.x, p2.y - p1.y);
            double n = d.norm();
            if (n > 0.0) {
                d.x /= n; d.y /= n;
            }
            return d;
        }
    };


    



    //string opt_infile = "G:/My Drive/LIGO/BPM/Ccode/test/test/images/img0.txt";
    


    //// read line segments from input file
    //std::vector<Segment> segs;
    //double x1, x2, y1, y2;
    //FILE* f = fopen(opt_infile.c_str(), "r");
    //if (!f) {
    //    fprintf(stderr, "Cannot open '%s'\n", opt_infile.c_str());
    //    return NULL;
    //}
    //int npoints = 0;
    //while (!feof(f)) {
    //    npoints++;
    //    k = fscanf(f, "%lf,%lf,%lf,%lf\n", &x1, &x2, &y1, &y2);
    //    if (k != 4) {
    //        fprintf(stderr, "Error in line %i: wrong format\n", npoints);
    //        return NULL;
    //    }
    //    segs.push_back(Segment(Point(x1, x2), Point(y1, y2)));
    //}
    //fclose(f);

    //// computation of condensed distance matrix
    //double* distmat = new double[(npoints * (npoints - 1)) / 2];
    //k = 0;
    //double sprod;
    //double d;
    //for (int i = 0; i < npoints; i++) {
    //    for (int j = i + 1; j < npoints; j++) {

    //        // line segment distance (cosine dissimilarity)
    //        Segment s1 = segs[i]; 
    //        Segment s2 = segs[j];
    //        sprod = s1.dir.x * s2.dir.x + s1.dir.y * s2.dir.y;
    //        d = 1 - sprod * sprod;

    //        if (d < 0.0)
    //            distmat[k] = 0;
    //        else
    //            distmat[k] = d;

    //        k++;
    //    }
    //}


    
    // read points from input file
    /*int k;
    std::vector<Point> points;
    double x1, y1;
    FILE* f = fopen(opt_infile.c_str(), "r");
    if (!f) {
        fprintf(stderr, "Cannot open '%s'\n", opt_infile.c_str());
        return;
    }
    int npoints = 0;
    while (!feof(f)) {
        npoints++;
        k = fscanf(f, "%lf,%lf\n", &x1, &y1);
        if (k != 2) {
            fprintf(stderr, "Error in line %i: wrong format\n", npoints);
            return;
        }
        points.push_back(Point(x1, y1));
    }
    fclose(f);*/
    

    // read image data
    int k;
    std::vector<Point_> points;
    std::vector<double> intensities;
    int tempx, tempy;
    double val;
    FILE* f = fopen(opt_infile.c_str(), "r");
    if (!f) {
        fprintf(stderr, "Cannot open '%s'\n", opt_infile.c_str());
        return false;
    }
    int npoints = 0;
    int id = 0;
    while (!feof(f)) {
        if (isImgFile1Col) {
            k = fscanf(f, "%lf\n", &val);
        }
        else
        {
            k = fscanf(f, "%i, %i, %lf\n", &tempx, &tempy, &val);
        }
        
        //if (k != 1) {
        //    fprintf(stderr, "Error in line %i: wrong format\n", npoints);
	//    cout << tempx << tempy << val << endl;
        //    return;
        //}
        if (val >= clusterNoiseThreshold) {
            points.push_back(Point_(xg[id], yg[id]));
            intensities.push_back(val);
            npoints++;
        }
        
        id++;
    }
    fclose(f);



    // computation of condensed distance matrix
    double* distmat = new double[(npoints * (npoints - 1)) / 2];
    k = 0;
    double d;
    for (int i = 0; i < npoints; i++) {
        for (int j = i + 1; j < npoints; j++) {

            // Euclidean distance
            Point_ p1 = points[i];
            Point_ p2 = points[j];
            d = sqrt((p1.x - p2.x)* (p1.x - p2.x) + (p1.y - p2.y)* (p1.y - p2.y));

            if (d < 0.0)
                distmat[k] = 0;
            else
                distmat[k] = d;

            k++;
        }
    }





    // clustering call
    int* merge = new int[2 * (npoints - 1)];
    double* height = new double[npoints - 1];
    hclust_fast(npoints, distmat, HCLUST_METHOD_SINGLE, merge, height);

    int* labels = new int[npoints];
    //cutree_k(npoints, merge, 2, labels);
    cutree_cdist(npoints, merge, height, clusterDist, labels);



    // print result
    /*string opt_outfile = "G:/My Drive/LIGO/BPM/Ccode/test/test/images/img0_out.txt";
    f = fopen(opt_outfile.c_str(), "w");
    if (!f) {
        fprintf(stderr, "Cannot open '%s'\n", opt_outfile.c_str());
        return NULL;
    }
    */
    //for (int i = 0; i < npoints; i++) {
    //    /*printf("%3.2f,%3.2f,%3.2f,%3.2f,%i\n",
    //        segs[i].p1.x, segs[i].p1.y, segs[i].p2.x, segs[i].p2.y, labels[i]);*/
    //    printf("%3.2f, %3.2f, %i\n",
    //        points[i].x, points[i].y, labels[i]);

    //    fprintf(f, "%3.2f, %3.2f, %i\n", points[i].x, points[i].y, labels[i]);
    //}
    //fclose(f);

    
    
    // find brightest cluster
    int labelmax = 0;
    for (int i = 0; i < npoints; i++) {
        if (labels[i] > labelmax) {
            labelmax = labels[i];
        }
    }
    labelmax++;

    
    double intensityMax = 0;
    int labelBrightid = labelmax + 1;
    std::vector<double> ilist(labelmax);
    std::vector<int> labelSize(labelmax);
    for (int i = 0; i < labelmax; i++) {
        ilist[i] = 0;
        labelSize[i] = 0;
    }

    for (int i = 0; i < npoints; i++) {
        id = labels[i];
        ilist[id] += intensities[i];
        labelSize[id]++;
        if (ilist[id] > intensityMax) {
            labelBrightid = id;
            intensityMax = ilist[id];
        }
    }


    int finalsize = labelSize[labelBrightid];
    double* xfg = new double[finalsize];
    double* yfg = new double[finalsize];

    int crosscheck = 0;
    double centroidx = 0;
    double centroidy = 0;
    for (int i = 0; i < npoints; i++) {
        if (labels[i] == labelBrightid) {
            xfg[crosscheck] = points[i].x;
            yfg[crosscheck] = points[i].y;
            centroidx += points[i].x;
            centroidy += points[i].y;

            crosscheck++;
        }
    }

    centroidx /= crosscheck;
    centroidy /= crosscheck;

    cout << "BPM: CentroidX = " << centroidx << endl;
    cout << "BPM: CentroidY = " << centroidy << endl;

    
    if (crosscheck != finalsize) {
        fprintf(stderr, "Largest cluster size doesn't match with labels!\n");
    }


    this->clusterXList = xfg;
    this->clusterYList = yfg;
    this->clusterListSize = finalsize;
    
    this->clusterXCentroid = centroidx;
    this->clusterYCentroid = centroidy;


    delete[] distmat;
    delete[] merge;
    delete[] height;
    delete[] labels;

    cout << "BPM: Data clustered" << endl;
    return true;
}

void BeamPosMon::findForeground()
{
    cout << "BPM: Finding foreground" << endl;

    // load saved data
    /*int k;
    int id;
    vector<int> result;
    string opt_infile = "G:/My Drive/LIGO/BPM/Ccode/test/test/images/0513_20WBeamDrifting/fg.txt";
    FILE* f = fopen(opt_infile.c_str(), "r");
    if (!f) {
        fprintf(stderr, "Cannot open '%s'\n", opt_infile.c_str());
        return;
    }
    while (!feof(f)) {

        k = fscanf(f, "%i\n", &id);
        result.push_back(id);
        
    }
    fclose(f);*/




    // uncomment real code below

    vector<int> polyid = convexHull(this->clusterXList, this->clusterYList, this->clusterListSize);

    int nvert = int(polyid.size());
    double* vertx = new double[nvert];
    double* verty = new double[nvert];


    double x, y, templ;
    for (int i = 0; i < nvert; i++) {
        x = clusterXList[polyid[i]];
        y = clusterYList[polyid[i]];

        templ = sqrt((x - clusterXCentroid) * (x - clusterXCentroid) + (y - clusterYCentroid) * (y - clusterYCentroid));
        vertx[i] = x + foregroundClusterPadding * (x - clusterXCentroid) / templ; // list of x coordinates that are vertices
        verty[i] = y + foregroundClusterPadding * (y - clusterYCentroid) / templ;
    }
    

    vector<int> result;

    for (int k = 0; k < imgr * imgc; k++) {
        x = xg[k];
        y = yg[k];

        int i, j, c = 0;
        for (i = 0, j = nvert - 1; i < nvert; j = i++) {
            if (((verty[i] > y) != (verty[j] > y)) &&
                (x < (vertx[j] - vertx[i]) * (y - verty[i]) / (verty[j] - verty[i]) + vertx[i]))
                c = ~c;
        }

        if (c)
            result.push_back(k);

    }
    delete[] vertx;
    delete[] verty;



    fgn = int(result.size());

    int* indexlist = new int[fgn];
    int* fgxlist = new int[fgn];
    int* fgylist = new int[fgn];
    //this->currfg = new double[fgn];
    for (int i = 0; i < fgn; i++) {
        indexlist[i] = result[i];
        fgxlist[i] = xg[result[i]];
        fgylist[i] = yg[result[i]];
    }

    this->fgid = indexlist;
    this->fgx = fgxlist;
    this->fgy = fgylist;



    // make foreground region white to verify
    /*string opt_outfile = "/data/wenxuan.jia/BPM/camera/SnapPy/07112022_fg.txt";
    double* img = new double[imgr*imgc];
    loadImageFromTxt(img, "/usr/local/home/controls/SnapPy/refImage/07112022.txt");
    for(int i = 0; i < fgn; i++){
	    img[fgid[i]] = 255;
    }
    saveImageToTxt(img, opt_outfile);*/



    cout << "BPM: Foreground pixel number = " << fgn << endl;
    
    cout << "BPM: Foreground found" << endl;
    return;
}

void BeamPosMon::findRefBeamParamKnownXY(double* firstImg, double refCentX, double refCentY)
{
    cout << "BPM: Finding reference beam param, given XY" << endl;


    double intnsty;
    int id;

    vector<double> rlist, ilist;
    vector<Point> pointlist;
    Point point;

    refFgPwr = 0;
    double pwr = 0;

    if (isScatMapReady) {
        cout << "BPM: Get beam parameter, scatter map loaded." << endl;
        for (int i = 0; i < sgn; i++) {
            id = sgid[i];
            if (firstImg[id] < saturation) {
                rlist.push_back(sqrt((sgx[i] - refCentX) * (sgx[i] - refCentX) + (sgy[i] - refCentY) * (sgy[i] - refCentY)));

                intnsty = firstImg[id] / sg[i]; // divide scatter map
                ilist.push_back(intnsty);

            }
        }
        
    }
    else
    {

        if (!fgx) {
            this->findForeground();

        }

        // remove saturated pixel and background pixel
        this->zeroSatPixel(firstImg);
        double* refFg = new double[fgn];
        this->cutForeground(firstImg, refFg);

        for (int i = 0; i < fgn; i++) {
            if (refFg[i] > 0) {
                rlist.push_back(sqrt((fgx[i] - refCentX) * (fgx[i] - refCentX) + (fgy[i] - refCentY) * (fgy[i] - refCentY)));
                ilist.push_back(refFg[i]);
            }
        }

        delete[] refFg;

    }



    // load sample data
    /*int k;
    double ri, ii;
    vector<double> rlist, ilist;
    string opt_infile = "G:/My Drive/LIGO/BPM/Ccode/test/test/images/gaussfit.txt";
    FILE* f = fopen(opt_infile.c_str(), "r");
    if (!f) {
    	fprintf(stderr, "Cannot open '%s'\n", opt_infile.c_str());
    	return;
    }
    int npoints = 0;
    int id = 0;
    while (!feof(f)) {

    	k = fscanf(f, "%lf, %lf\n", &ri, &ii);
    	rlist.push_back(ri);
    	ilist.push_back(ii);
    	npoints++;

    }
    fclose(f);*/

    int npar = 2; // I0, w
    double* par = new double[npar];
    par[0] = initialGuessI0;
    par[1] = initialGuessW;

    int ny = int(ilist.size());
    double* y = &ilist[0]; // y = f(x). x is rlist
    double* dysq = NULL;

    void* fdata = &rlist[0]; // the indepent variable x
    
    //cout << error_func(par, ny, y, NULL, BeamPosMon::LMfunc_calcI, fdata) << endl;

    LMstat* lmstat = new LMstat;
    lmstat->verbose = lmstat_verbose;
    lmstat->max_it = lmstat_max_it;
    lmstat->init_lambda = lmstat_init_lambda;
    lmstat->up_factor = lmstat_up_factor;
    lmstat->down_factor = lmstat_down_factor;
    lmstat->target_derr = lmstat_target_derr;
    int success = levmarq(npar, par, ny, y, dysq, this->LMgradientDelta, &LMfunc_calcIKnownXY, &LMgrad_calcIKnownXY, fdata, lmstat);

    cout << "BPM: Beam parameter found" << endl;
    cout << "BPM: Ref X = " << par[0] << endl;
    cout << "BPM: Ref Y = " << par[1] << endl;
    //cout << lmstat->final_derr << endl;
    //cout << lmstat->final_err << endl;
    //cout << lmstat->final_it << endl;

    refX = refCentX;
    refY = refCentY;
    refI0 = par[0];
    refw = par[1];


    delete lmstat;
    //delete[] refFg;
    delete[] par;
    return;
}


double BeamPosMon::LMfunc_calcIKnownXY(double* paramToFit, int i, void* rlist)
{
    double I0 = paramToFit[0];
    double w = paramToFit[1];

    double* dataptr = (double*)rlist;

    return I0 * exp(-2 * dataptr[i] * dataptr[i] / (w * w));
}

void BeamPosMon::LMgrad_calcIKnownXY(double* gradient, double* paramToFit, int k, void* rlist, double LMgradientDelta)
{
    int npar = 2;
    double* paramplusdelta = new double[npar];

    for (int i = 0; i < npar; i++) {
        for (int j = 0; j < npar; j++) {
            paramplusdelta[j] = paramToFit[j];
        }
        paramplusdelta[i] = paramToFit[i] * (1 + LMgradientDelta);
        gradient[i] = (LMfunc_calcIKnownXY(paramplusdelta, k, rlist) - LMfunc_calcIKnownXY(paramToFit, k, rlist)) / paramToFit[i] * LMgradientDelta;
    }
    
    delete[] paramplusdelta;
    return;
}

void BeamPosMon::findRefBeamParam(double* img)
{
    cout << "BPM: Finding reference beam param" << endl;

    //double* img = new double[imgr * imgc];

    //loadImageFromTxt(img, opt_infile);


    double refCentX = 0;
    double refCentY = 0;
    double intnsty;
    int id; 

    vector<double> ilist;
    vector<Point> pointlist;
    Point point;

    refFgPwr = 0;
    double pwr = 0;

    if (isScatMapReady) {
        cout << "BPM: Get beam parameter, scatter map loaded." << endl;
        for (int i = 0; i < sgn; i++) {
            id = sgid[i];
            if (img[id] < saturation) {
                point.x = sgx[i];
                point.y = sgy[i];
                pointlist.push_back(point);
                intnsty = img[id] / sg[i]; // divide scatter map
                ilist.push_back(intnsty);

                refFgPwr += img[id];
                pwr += intnsty;
                refCentX += intnsty * sgx[i];
                refCentY += intnsty * sgy[i];
            }
        }
        refCentX /= pwr;
        refCentY /= pwr;
    }
    else
    {
        if (!fgx) {
            this->findForeground();
        }


        // start ref foreground centroid
        double* refFg = new double[fgn];
        this->cutForeground(img, refFg);
        
        for (int i = 0; i < fgn; i++) {
            refCentX += refFg[i] * fgx[i];
            refCentY += refFg[i] * fgy[i];
            refFgPwr += refFg[i];
        }
        refCentX /= refFgPwr;
        refCentY /= refFgPwr;


        // remove saturated pixel and background pixel
        this->zeroSatPixel(img);
        this->cutForeground(img, refFg);

        
        for (int i = 0; i < fgn; i++) {
            //cout << "refFg[" << i << "] = " << refFg[i] << endl;
            if (refFg[i] > 0) {
                point.x = fgx[i];
                point.y = fgy[i];
                pointlist.push_back(point);
                ilist.push_back(refFg[i]);

            }
        }

        delete[] refFg;

    }

    

    // load sample data
    /*int k;
    double ri, ii;
    vector<double> rlist, ilist;
    string opt_infile = "G:/My Drive/LIGO/BPM/Ccode/test/test/images/gaussfit.txt";
    FILE* f = fopen(opt_infile.c_str(), "r");
    if (!f) {
        fprintf(stderr, "Cannot open '%s'\n", opt_infile.c_str());
        return;
    }
    int npoints = 0;
    int id = 0;
    while (!feof(f)) {

        k = fscanf(f, "%lf, %lf\n", &ri, &ii);
        rlist.push_back(ri);
        ilist.push_back(ii);
        npoints++;

    }
    fclose(f);*/

    int npar = 4; // I0, w, x, y
    double* par = new double[npar];
    par[0] = initialGuessI0;
    par[1] = initialGuessW;
    par[2] = refCentX;
    par[3] = refCentY;

    int ny = int(ilist.size());
    double* y = &ilist[0]; // y = f(x). x is rlist
    double* dysq = NULL;

    void* fdata = &pointlist[0]; // the indepent variable x

    //cout << error_func(par, ny, y, NULL, BeamPosMon::LMfunc_calcI, fdata) << endl;

    LMstat* lmstat = new LMstat;
    lmstat->verbose = lmstat_verbose;
    lmstat->max_it = lmstat_max_it;
    lmstat->init_lambda = lmstat_init_lambda;
    lmstat->up_factor = lmstat_up_factor;
    lmstat->down_factor = lmstat_down_factor;
    lmstat->target_derr = lmstat_target_derr;
    int success = levmarq(npar, par, ny, y, dysq, this->LMgradientDelta, &LMfunc_calcI, &LMgrad_calcI, fdata, lmstat);

    cout << "BPM: Beam parameter found" << endl;
    cout << "BPM: Ref I0 = " << par[0] << endl;
    cout << "BPM: Ref w = " << par[1] << endl;
    cout << "BPM: Ref X = " << par[2] << endl;
    cout << "BPM: Ref Y = " << par[3] << endl;
    //cout << par[0] << endl;
    //cout << par[1] << endl;
    //cout << lmstat->final_derr << endl;
    //cout << lmstat->final_err << endl;
    //cout << lmstat->final_it << endl;

    
    refI0 = par[0];
    refw = par[1];
    refX = par[2];
    refY = par[3];


    delete lmstat;
    //delete[] img;
    delete[] par; 
    return;
}


double BeamPosMon::LMfunc_calcI(double* paramToFit, int i, void* data)
{
    Point* dataptr = (Point*)data;
    double xdata = dataptr[i].x;
    double ydata = dataptr[i].y;

    double I0 = paramToFit[0];
    double w = paramToFit[1];
    double x = paramToFit[2];
    double y = paramToFit[3];


    return I0 * exp(-2 * ((xdata - x) * (xdata - x) + (ydata - y) * (ydata - y)) / (w * w));
}

void BeamPosMon::LMgrad_calcI(double* gradient, double* paramToFit, int k, void* data, double LMgradientDelta)
{
    int npar = 4;
    double* paramplusdelta = new double[npar];

    for (int i = 0; i < npar; i++) {
        for (int j = 0; j < npar; j++) {
            paramplusdelta[j] = paramToFit[j];
        }
        paramplusdelta[i] = paramToFit[i] * (1 + LMgradientDelta);
        gradient[i] = (LMfunc_calcI(paramplusdelta, k, data) - LMfunc_calcI(paramToFit, k, data)) / paramToFit[i] * LMgradientDelta;
    }

    delete[] paramplusdelta;
    return;
}

void BeamPosMon::find1stScatterMap(double* img)
{
    cout << "BPM: Finding current scatter map" << endl;
    //double* img = new double[imgr * imgc];
    //loadImageFromTxt(img, opt_infile);
    //this->zeroSatPixel(img);

    double* refFg = new double[fgn];
    this->cutForeground(img, refFg);

    vector<int> sgidvec; // index of xg, yg that has scatter map data
    vector<double> fgvec; // vector of fg that's not saturated
    for (int i = 0; i < fgn; i++) {
        if (refFg[i] > 0) {
            sgidvec.push_back(fgid[i]);
            fgvec.push_back(refFg[i]);
        }
    }

    sgn = int(sgidvec.size());
    sgid = new int[sgn];
    sgx = new int[sgn];
    sgy = new int[sgn];
    sg = new double[sgn];

    for (int i = 0; i < sgn; i++) {
        sgid[i] = sgidvec[i];
        sgx[i] = xg[sgidvec[i]];
        sgy[i] = yg[sgidvec[i]];

        sg[i] = fgvec[i] / I_00(sgx[i]-refX, sgy[i]-refY, refI0, refw);
    }



    // print results
    /*string opt_outfile = "G:/My Drive/LIGO/BPM/Ccode/test/test/images/0513_20WBeamDrifting/sg.txt";
    FILE* f = fopen(opt_outfile.c_str(), "w");
    if (!f) {
        fprintf(stderr, "Cannot open '%s'\n", opt_outfile.c_str());
        return;
    }

    for (int i = 0; i < sgn; i++) {

        fprintf(f, "%i, %i, %lf\n", sgx[i], sgy[i], sg[i]);
    }
    fclose(f);*/



    //delete[] img;
    delete[] refFg;
    cout << "BPM: current scatter map found" << endl;
    return;
}

void BeamPosMon::calcScatterMap(string imgAddr)
{
    //isImgFile1Col = true;
    if (this->loadScatterMap(imgAddr)) {
        isScatMapReady = true;
        return;
    }
    

    //double* img = new double[480 * 640];
    int i = 1;
    auto temp = std::to_string(i);
    string opt_infile = imgAddr + refImgFileNamePrefix + temp + ".txt";
    string opt_outfile = imgAddr + scatMapFileName + temp + ".txt";

    vector<vector<double>> sgall(imgr*imgc); // a 480*640 vector to store all possible scatter map data

    
    while (this->clusterData(opt_infile)) {
        
        this->findForeground();

        this->loadImageFromTxt(image, opt_infile);

        this->findRefBeamParam(image); // this would set saturated pixel to 0

        this->find1stScatterMap(image);

        if (saveEachScatterMap) {
            this->saveScatterMap(opt_outfile);
        }

        // accumulate the scatter map
        for (int s = 0; s < sgn; s++) {
            sgall[sgid[s]].push_back(sg[s]);
        }



        i++;
        temp = std::to_string(i);
        opt_infile = imgAddr + refImgFileNamePrefix + temp + ".txt";
        opt_outfile = imgAddr + scatMapFileName + temp + ".txt";
    }



    // calculate the averaged scatter map
    vector<int> sgidvec; // vector version of sgid
    vector<double> sgvec; // vector version of sg
    double val;
    int sz;

    for (int i = 0; i < imgr * imgc; i++) {
        sz = sgall[i].size();
        if (sz > 0) {
            sgidvec.push_back(i);
            

            // calculate average of scatter values
            val = 0;
            for (int j = 0; j < sz; j++) {
                val += sgall[i][j];
            }
            val = val / sz;



            sgvec.push_back(val);
        }
        
    }


    // re-assemble everything in pointers
    delete[] sgid, sgx, sgy, sg;

    sgn = int(sgidvec.size());
    sgid = new int[sgn];
    sgx = new int[sgn];
    sgy = new int[sgn];
    sg = new double[sgn];

    for (int i = 0; i < sgn; i++) {
        sgid[i] = sgidvec[i];
        sgx[i] = xg[sgidvec[i]];
        sgy[i] = yg[sgidvec[i]];

        sg[i] = sgvec[i];
    }


    // save final scatter map to scatMap.txt
    opt_outfile = imgAddr + scatMapFileName + ".txt";
    this->saveScatterMap(opt_outfile);

    isScatMapReady = true;

    //delete[] img;
    return;
}

void BeamPosMon::zeroSatPixel(double* img)
{
    //cout << "BPM: Zeroing saturated pixel" << endl;
    for (int i = 0; i < imgr * imgc; i++) {
        //cout << "i = " << i << ", img[i] = " << img[i] << endl;
        if (img[i] >= saturation) {
            img[i] = 0;
        }
    }

    return;
}

void BeamPosMon::cutForeground(double* img, double* fg)
{
    for (int i = 0; i < fgn; i++) {
        fg[i] = img[fgid[i]];
    }

    return;
}

void BeamPosMon::getPos_deconvolveFirstOrder(double* result, double* img, double deltaxFor10, double deltayFor01)
{
    
    double pos, x, y, sum10, sum01, sumimg10, sumimg01, simul;
    vector<double> coeff10, coeff01;
    for (int i = 0; i < posGrid.size(); i++) {
        pos = posGrid[i];
        sum10 = 0; sum01 = 0; 

        for (int s = 0; s < sgn; s++) {
            x = sgx[s];
            y = sgy[s];
            simul = round(I_00(x - (refX + pos), y - refY, refI0, refw));
            if (simul < saturation)
                sum10 += I_10(x - refX, y - refY, refI0, refw, deltaxFor10) * sg[s] * simul;

            simul = round(I_00(x - refX, y - (refY + pos), refI0, refw));
            if (simul < saturation)
                sum01 += I_01(x - refX, y - refY, refI0, refw, deltayFor01) * sg[s] * simul;

        }
        coeff10.push_back(sum10);
        coeff01.push_back(sum01);

    }

    sumimg10 = 0; sumimg01 = 0;
    for (int s = 0; s < sgn; s++) {
        x = sgx[s];
        y = sgy[s];
        sumimg10 += I_10(x - refX, y - refY, refI0, refw, deltaxFor10) * img[sgid[s]];
        sumimg01 += I_01(x - refX, y - refY, refI0, refw, deltayFor01) * img[sgid[s]];
    }

    result[0] = interp1(coeff10, posGrid, sumimg10);
    result[1] = interp1(coeff01, posGrid, sumimg01);
    return;
}

double BeamPosMon::getPosX_deconvolveFirstOrder(double* img, double deltaxFor10)
{
    

     
    double pos, x, y, sum10, sum01, sumimg10, sumimg01, simul;
    vector<double> coeff10, coeff01;
    //cout << "BPM: Getting X" << endl;
    for (int i = 0; i < posGrid.size(); i++) {
        pos = posGrid[i];
        sum10 = 0; sum01 = 0;

        for (int s = 0; s < sgn; s++) {
            x = sgx[s];
            y = sgy[s];
            simul = round(I_00(x - (refX + pos), y - refY, refI0, refw));
            if (simul < saturation)
                sum10 += I_10(x - refX, y - refY, refI0, refw, deltaxFor10) * sg[s] * simul;

            /*simul = round(I_00(x - refX, y - (refY + pos), refI0, refw));
            if (simul < saturation)
                sum01 += I_01(x - refX, y - refY, refI0, refw, deltayFor01) * sg[s] * simul;*/

        }
        coeff10.push_back(sum10);
        //coeff01.push_back(sum01);
        //cout << "i = " << i << endl;
    }

    //cout << "coeff10 done" << endl;
    sumimg10 = 0; sumimg01 = 0;
    for (int s = 0; s < sgn; s++) {
        x = sgx[s];
        y = sgy[s];
        sumimg10 += I_10(x - refX, y - refY, refI0, refw, deltaxFor10) * img[sgid[s]];
        //sumimg01 += I_01(x - refX, y - refY, refI0, refw, deltayFor01) * img[sgid[s]];
    }
    //cout << "sumimg10 done" << endl;
    double result = interp1(coeff10, posGrid, sumimg10); 
    //cout << "BPM: Beam position X = " << result << endl;
    return result;
}

double BeamPosMon::getPosY_deconvolveFirstOrder(double* img, double deltayFor01)
{
    double pos, x, y, sum10, sum01, sumimg10, sumimg01, simul;
    vector<double> coeff10, coeff01;
    for (int i = 0; i < posGrid.size(); i++) {
        pos = posGrid[i];
        sum10 = 0; sum01 = 0;

        for (int s = 0; s < sgn; s++) {
            x = sgx[s];
            y = sgy[s];
            /*simul = round(I_00(x - (refX + pos), y - refY, refI0, refw));
            if (simul < saturation)
                sum10 += I_10(x - refX, y - refY, refI0, refw, deltaxFor10) * sg[s] * simul;*/

            simul = round(I_00(x - refX, y - (refY + pos), refI0, refw));
            if (simul < saturation)
                sum01 += I_01(x - refX, y - refY, refI0, refw, deltayFor01) * sg[s] * simul;

        }
        //coeff10.push_back(sum10);
        coeff01.push_back(sum01);

    }

    sumimg10 = 0; sumimg01 = 0;
    for (int s = 0; s < sgn; s++) {
        x = sgx[s];
        y = sgy[s];
        //sumimg10 += I_10(x - refX, y - refY, refI0, refw, deltaxFor10) * img[sgid[s]];
        sumimg01 += I_01(x - refX, y - refY, refI0, refw, deltayFor01) * img[sgid[s]];
    }

    return interp1(coeff01, posGrid, sumimg01);
}
