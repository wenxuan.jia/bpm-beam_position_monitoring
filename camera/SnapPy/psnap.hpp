#ifndef PYLONINCLUDES
#include <pylon/PylonIncludes.h>
#include <pylon/gige/BaslerGigECamera.h>
#define PYLONINCLUDES
#endif

#include <CGrabBuffer.hpp>

#include <ostream>
#include <string>

typedef Pylon::CBaslerGigECamera Camera_t;
using namespace std;

//Python includes (Python and Numpy)
#include "Python.h"

//#include <ImageLib.h>


//Beam Position Monitoring includes
#include "fastcluster.h"
//#include "fastcluster.cpp"
#include "convexHull.h"
#include "levmarq.h"
//#include "levmarq.cpp"
#include "BeamPosMon.h"


//*********************************
//CameraStruct 
typedef struct
{
  Camera_t Camera;
  Camera_t::StreamGrabber_t StreamGrabber;
  std::vector<CGrabBuffer*> BufferList;
  GrabResult Frame;
  BeamPosMon monitor;
  int recordImgid = 1;
} CameraStruct;
//*********************************

// Creates and opens a camera object for the first available GigE camera, used before "openCamera"
bool getCamera(CameraStruct* myCamera);

// Creates a camera object for a GigE camera based on IP, used before "openCamera"
bool getCameraByAddr(CameraStruct* myCamera, char* IpAddress);

// Opens the camera for use, used after "getCamera" or "getCameraByAddr", before "setupCamera"
int openCamera(CameraStruct* myCamera);

//Closes the camera for use, used after "stopCamera"
void closeCamera(CameraStruct* myCamera);

//Sets the parameters of the camera, used after "openCamera", before "startCamera"
bool setupCamera(CameraStruct* myCamera, int height_value, int width_value, int x_value, int y_value, int exposure_time, int analog_gain, char* PixelFormat, int black_level);

//Starts the camera producing streaming data, used after "setupCamera", before "snapCamera" or "stopCamera"
bool startCamera(CameraStruct* myCamera, int max_frame_rate);

//Stops the cameras streaming data, used after "startCamera" or "snapCamera"
bool stopCamera(CameraStruct* myCamera);

//Gets the data into a frame, used after "startCamera"
int snapCamera(CameraStruct* myCamera);






// Record images for scatter map generation
void recordForScatMap(CameraStruct* myCamera, char* saveAddr);

// Saves the reference image
void getBPMRefImage(CameraStruct* myCamera, char* refImageAddr);

// Finds the foreground region for beam position monitor
void findBPMForeground(CameraStruct* myCamera, char* imgAdrForForeground);

// Finds beam parameter for beam position monitor
void findBPMBeamParameter(CameraStruct* myCamera, char* imgAdrForBeamParam);

// Finds scatter map for beam position monitor
void findBPMScatterMap(CameraStruct* myCamera, char* imgAdrForScatMap);

//Releases the StreamGrabber result frame, must be run once after each time snapCamera is run. Can't run getFrame after this is run, only after snapCamera.
bool releaseCameraBuffer(CameraStruct* myCamera);

// Access frame buffer data at indicated offset from zero (position)
// Since this pointing to the raw data, running snapCamera before copying
// will change the data

PyObject* getFrame(CameraStruct* myCamera);

double* getFramePtr(CameraStruct* myCamera);

// Gets beam positions
double getBeamPosX(CameraStruct* myCamera);
double getBeamPosY(CameraStruct* myCamera);
