function img = invalidate(img_s) % invalidate saturated or dead pixels
    
    saturation = 245.5;
    img = img_s;
    img(img >= saturation) = NaN;
    
    


end