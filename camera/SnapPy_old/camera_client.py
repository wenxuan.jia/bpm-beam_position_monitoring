#!/usr/bin/env python

import sys
import os
import getopt
import ConfigParser
import math

def usage():
    print "-h, --help          Displays this message"
    print "-c, --config        Uses the settings in the config file listed"
    print "-s, --scaling       Creates a display which scales to window size"
    print "-a, --ascii         Creates an ascii art representation of the image"
    print "-r, --rotation      Counter clockwise rotation in degrees"
    print "-i, --image         Image to overlay video"

#Default
host = "127.0.0.1"
port = "5000"


#Parse command line options
try:
   opts,args = getopt.getopt(sys.argv[1:],"hc:sar:i:",["help","config=","scaling","ascii","rotation=","image="])
except getopt.GetoptError:
   usage()
   sys.exit(2)

if len(opts) == 0:
    usage()
    sys.exit()

#Default settings
ascii_display = False
scaling_display = False
rotation = None
image = None

for o, a in opts:

    if o in ("-h", "--help"):
        usage()
        sys.exit()
    elif o in ("-c", "--config"):
        configFile = str(a)
    elif o in ("-s", "--scaling"):
        scaling_display = True
    elif o in ("-a", "--ascii"):
        ascii_display = True
    elif o in ("-r", "--rotation"):
        if a.strip() == "":
            rotation = False
        else:
            rotation = str(a)
    elif o in ("-i", "--image"):
        if a.strip() == "":
            image = False
        else:
            image = str(a)
    else:
        usage()
        sys.exit()

#Create config parser and grab necessary configuration information
config = ConfigParser.ConfigParser()
config.read(configFile)
port = config.get('No Reload Camera Settings','Multicast Port')
multicastGroup = config.get('No Reload Camera Settings','Multicast Group')
if image == None:
    print "image false"
    if config.has_option('Client Settings','SVG Image'):
        image = config.get('Client Settings','SVG Image')
if rotation == None:
    print "rotation false"
    if config.has_option('Client Settings','Rotation in degrees'):
        rotation = str(config.getfloat('Client Settings','Rotation in degrees')*math.pi/180.0)


#Setup command string to be run
#Ascii overrides scaling because the gstreamer ascii art plugin scales as well.

rotation_string = ""
if rotation:
    rotation_string = "! rotate angle=" + rotation + " ! ffmpegcolorspace "
image_string = ""
if image:
    image_string = "! rsvgoverlay location=" + image + " ! ffmpegcolorspace "


if ascii_display:
    commandString = ''.join(["gst-launch udpsrc multicast-group=",str(multicastGroup)," port=",str(port), " ! application/x-rtp, payload=127 ! rtph264depay ! ffdec_h264 ! ffmpegcolorspace ! ffmpegcolorspace " + image_string + rotation_string + " ! aasink sync=false"])
elif scaling_display:
    commandString = ''.join(["gst-launch udpsrc multicast-group=",str(multicastGroup)," port=",str(port), " ! application/x-rtp, payload=127 ! rtph264depay ! ffdec_h264 ! ffmpegcolorspace " + image_string + rotation_string + " ! xvimagesink sync=false"])
else:
    commandString = ''.join(["gst-launch udpsrc multicast-group=",str(multicastGroup)," port=",str(port), " ! application/x-rtp, payload=127 ! rtph264depay ! ffdec_h264 ! ffmpegcolorspace " + image_string + rotation_string + " ! ximagesink sync=false"])

#Run the display
print commandString
os.system(commandString)
