#pragma once
#ifndef BEAMPOSMON_H
#define BEAMPOSMON_H

#include <iostream>

#include <math.h>
#include <string.h>

#include <string>
#include <vector>
#include <stdio.h>
#include <algorithm>

#include "fastcluster.h"
#include "levmarq.h"
#include "convexHull.h"

//#include "fastcluster.cpp"
//#include "levmarq.cpp"
//#include "convexHull.cpp"


using namespace std;

//class Image {
//public:
//    double* x; // list of x values; 480x640 length
//    double* y; // list of y values;
//    double* I; // intensity data
//    Image(double* xx, double* yy, double* II) { x = xx; y = yy; I = II; }
//    Image(const Image& p) { x = p.x; y = p.y; I = p.I; }
//
//    double invalidate() {
//
//
//    }
//
//};

typedef struct {
    double x;
    double y;
} Point;



class BeamPosMon
{
public:
    BeamPosMon();
    //~BeamPosMon();

    // Toolbox functions
    static double I_00(double x, double y, double I0, double w); // TEM00 intensity
    static double I_10(double x, double y, double I0, double w, double deltax); // TEM 10 intensity
    static double I_01(double x, double y, double I0, double w, double deltay); // TEM 01 intensity
    static double interp1(vector<double> x, vector<double> y, double xq); // 1D interpolation
    void loadImageFromTxt(double* img, string opt_infile); // load images
    void saveImageToTxt(double* img, string opt_outfile); // save reference image to txt file
    
    // Get foreground features and scatter map
    void clusterData(string opt_infile); // get int* clusterLabel
    void findForeground(); // find the foreground indices
    void findRefBeamParamKnownXY(double* firstImg, double refCentX, double refCentY); // find the beam parameters with known beam position
    static double LMfunc_calcIKnownXY(double* paramToFit, int i, void* rlist); // LM func. paramToFit = [I0, w]
    static void LMgrad_calcIKnownXY(double* gradient, double* paramToFit, int i, void* rlist, double LMgradientDelta); // LM grad. paramToFit = [I0, w]
    void findRefBeamParam(string opt_infile); // find the beam parameters and beam position
    static double LMfunc_calcI(double* paramToFit, int i, void* data); // LM func. paramToFit = [I0, w, x, y]
    static void LMgrad_calcI(double* gradient, double* paramToFit, int i, void* data, double LMgradientDelta); // LM grad. paramToFit = [I0, w, x, y]
    void find1stScatterMap(string opt_infile); // find scatter map

    // Pre-processing
    void zeroSatPixel(double* img); // zero all saturated pixels
    void cutForeground(double* img, double* fg); // take out foreground

    // Get beam position
    void getPos_deconvolveFirstOrder(double* result, double* img, double deltaxFor10, double deltayFor01); // find relative x, y beam pos using deconvolution first-order
    double getPosX_deconvolveFirstOrder(double* img, double deltaxFor10); // find relative x beam pos using deconvolution first-order
    double getPosY_deconvolveFirstOrder(double* img, double deltayFor01); // find relative y beam pos using deconvolution first-order


    // some data that has to be public
    int fgn = 0; // length(fgid)
    int* fgid = NULL; // obtained from void findFeatures()
    int sgn = 0; // length(sgid), which is <= fgn
    int imgr; // image height, 480
    int imgc; // image column, 640
    double saturation; // saturation level
    double clusterDist; // distance to cut cluster tree
    double clusterNoiseThreshold; // threshold to do clustering
    double foregroundClusterPadding; // padding of convex hull of brightest cluster, in pixels

    double initialGuessI0; // initial guess of beam intensity I0
    double initialGuessW; // initial guess of beam radius w
    double LMgradientDelta; // delta x used to find gradient (f(x+deltax)-f(x))/deltax
    int lmstat_verbose; // LM algorithm print every iteration, 0 is off, 1 is on
    int lmstat_max_it; // LM algorithm max iteration allowed
    double lmstat_init_lambda; // LM algorithm damping factor
    double lmstat_up_factor; // LM algorithm up factor
    double lmstat_down_factor; // LM algorithm down factor
    double lmstat_target_derr; // LM algorithm target differential error
    int lmstat_final_it = 0; // LM algorithm final iterations used
    double lmstat_final_err = 0; // LM algorithm final total mean-squared error
    double lmstat_final_derr = 0; // LM algorithm final differential error

    double refX = 0; // reference beam position X
    double refY = 0; // reference beam position Y
    double refI0 = 0; // reference beam center intensity I0
    double refw = 0; // reference beam radius w

    vector<double> posGrid; // range for deconvolution interpolation {-5, 0, 5}

private:

    int* xg; // x coordinate of the image, global
    int* yg; // y coordinate of the image, global


    //double* imgForCluster; // image used for clustering
    //int* clusterLabel; // obtained from void clusterData()

    double clusterXCentroid = 0;
    double clusterYCentroid = 0;;
    double* clusterXList = NULL; // List of x coordinates from clustered beam spot
    double* clusterYList = NULL; // List of y coordinates from clustered beam spot
    int clusterListSize = 0;

    
    int* fgx = NULL; // list of x that are in foreground
    int* fgy = NULL; // list of y that are in foreground
    //double* currfg; // foreground of current image

    double refFgPwr = 0; // total power in ref foreground

    int* sgid = NULL; // obtained from findScatterMap(). sgid \in fgid
    int* sgx = NULL; // list of x that are in scatter map
    int* sgy = NULL; // list of y that are in scatter map
    double* sg = NULL; // actual scatter map data
    

};

#endif // !BEAMPOSMON_H
