#!/usr/bin/env python
'''camera_server.py

This script loads a config file (default: pcameraSettings.ini),
then takes images from the indicated Basler camera and
broadcasts the images to clients connected via multicast UDP.  It uses
gstreamer as the video pipeline.'''

#----------------------------------------------------------
# start python imports
import sys

#Setup standard CDS enironment (not that we use it at the moment)
sys.path.append('/ligo/cdscfg')
import stdenv as cds;

cds.INIT_ENV()

#Basic modules for handling os, command line, and kill signals, and logging
import os
import getopt
import signal
import logging
import logging.handlers
import ConfigParser

#Imports for calculations and image processing/storing
import numpy
import scipy
from scipy import ndimage
from numpy import greater
from numpy import ogrid
import Image
import time
import copy

#Imports to make threading possible
import thread
import threading

#Imports for gstreamer
import pygst
pygst.require("0.10") #Probably don't need this
import gst
import gobject

gobject.threads_init()

#Custom module imports wrapping the basler camera API
import psnap
import pcamerasrc

#Imports for EPICS communications to the user
import epics
from epics import PV
#--------------------------------------------------------------
# Functions

#Logging filter function to help reduce spam
#Rejects duplicate messages after the first in any given time span (i.e. 1 second)
#After time span passes, indicates how many duplicate messages were rejected and starts counting again
class SingleCopyFilter(logging.Filter):

    def __init__(self):
        self.timer = time.time()
        self.messageBuffer = []
        self.messageCount = 0

    def filter(self, record):
        #This sets the time between sending messages
        if time.time() - self.timer > 1.0:
            self.messageBuffer = []
            self.timer = time.time()
            record.msg = 'Duplicate messages in the previous second: ' + str(self.messageCount) + ' ... ' + str(record.getMessage())
            self.messageCount = 0
        elif record.getMessage() in self.messageBuffer:
            self.messageCount = self.messageCount + 1
            return 0
        self.messageBuffer.append(record.getMessage())
        return 1

#------------------------------------------------------------------

#Print help function
def usage():
    print "-h, --help          displays this message"
    print "-c, --config=FILE   config file (\"pcam.ini\")"
    print "-d, --daemon        fork and run in the background"
    print "-p, --pidfile       location of the pid file"
    print "-l, --logfiledir    directory to place the log files"

#-------------------------------------------------------------------
#Definition of a communications and fitting thread which will run "parallel" with the
#video broadcast thread. Global Interpreter Lock (GIL) prevents pure
#python code from true threading, but the c code of gstreamer does
#thread properly (so the encoder will happily be working in its own
#thread).

#Handles communication via epics with ezcaread/write
class CommunicationThread(threading.Thread):
    #Initialization call of the CommunicationThread
    def __init__(self, mainThread):
        #Handing arrays between the two threads to act as communication paths
        self.callingThread = mainThread
        self.config = mainThread.config
        self.playmode = mainThread.playmode
        self.numpyArray = mainThread.numpyArray
        self.handOffFlag = mainThread.handOffFlag
        self.source = mainThread.source
        self.text = mainThread.text
        self.time = mainThread.time
        self.xmatrix = mainThread.xmatrix
        self.ymatrix = mainThread.ymatrix
        self.snapShotCount = 1
        self.archiveTime = time.time()

        #Setup comm logger
        try:
            if (self.config.get('Camera Settings','Log File Directory') != 'None') and (os.path.isdir(self.config.get('Camera Settings','Log File Directory'))):
                logFileName = self.config.get('Camera Settings','Log File Directory').strip() + '/' + os.path.basename(self.config.get('Camera Settings','iniFile').rstrip('.ini')) + '_comm.log'
                self.logger = logging.getLogger('comm')
                self.logger.setLevel(self.config.getint('Camera Settings','Log File Level'))
                handler = logging.handlers.TimedRotatingFileHandler(logFileName,when='W6',delay=True)
                handler.setLevel(self.config.getint('Camera Settings','Log File Level'))
                formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
                handler.setFormatter(formatter)
                self.logger.addHandler(handler)
                self.logger.addFilter(SingleCopyFilter())
            else:
                sys.stderr.write('Could not find log directory \'' + self.config.get('Camera Settings','Log File Directory') + '\'\n')
                self.logger = logging.getLogger('comm')
                self.logger.setLevel(self.config.getint('Camera Settings','Log File Level'))
                handler = logging.StreamHandler()
                handler.setLevel(self.config.getint('Camera Settings','Log File Level'))
                formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
                handler.setFormatter(formatter)
                self.logger.addHandler(handler)
                self.logger.addFilter(SingleCopyFilter())

        except:
            self.logger = logging.getLogger('comm')
            self.logger.setLevel(self.config.getint('Camera Settings','Log File Level'))
            handler = logging.StreamHandler()
            handler.setLevel(self.config.getint('Camera Settings','Log File Level'))
            formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
            handler.setFormatter(formatter)
            self.logger.addHandler(handler)
            self.logger.addFilter(SingleCopyFilter())

        #End comm logger setup

        #Call threading initialization
        threading.Thread.__init__(self)

    #----------------------------------------------------------
    #CommunicationThread function which has a while loop which continuously runs
    def run(self):

        #JCB: To Do: This is ugly to set values to -1, need to come up with a better method
        #Default values - negative value makes it obvious no calculation is being done
        #Since these should always be positive
        comX = -1
        comY = -1
        totalLight = -1

        baseChannelName = self.config.get('No Reload Camera Settings','Base Channel Name')

        #Setup epics communication
        autoExposureChannelPV = PV(baseChannelName+'_AUTO')
        exposureChannelPV = PV(baseChannelName+'_EXP')
        reloadChannelPV = PV(baseChannelName+'_RELOAD')
        snapFileNameChannelPV = PV(baseChannelName+'_FILE')
        snapshotChannelPV = PV(baseChannelName+'_SNAP')
        comXChannelPV = PV(baseChannelName+'_X')
        comYChannelPV = PV(baseChannelName+'_Y')
        varXChannelPV = PV(baseChannelName+'_WX')
        varYChannelPV = PV(baseChannelName+'_WY')
        #varXYChannelPV = PV(self.config.get('Camera Settings','varXYChannel')+'_XY')
        sumChannelPV = PV(baseChannelName+'_SUM')
        archiveResetPV = PV(baseChannelName+'_ARCHIVE_RESET')
        archiveTimePV = PV(baseChannelName +'_ARCHIVE_INTERVAL')

        #While told to play, keep playing video
        while self.playmode[0]:
            #Handles text overlays
            if (self.config.getboolean('Camera Settings','Name Overlay') | self.config.getboolean('Camera Settings','Time Overlay') | self.config.getboolean('Camera Settings','Calculation Overlay')):
                overlayString = ''
                if self.config.getboolean('Camera Settings','Name Overlay'):
                    overlayString = overlayString + " " + str(self.config.get('Camera Settings','Camera Name'))
                if self.config.getboolean('Camera Settings','Time Overlay'):
                    self.time = time.time()
                    overlayString = overlayString + " At " + time.strftime('%Y-%m-%d-%H-%M-%S') 
                if self.config.getboolean('Camera Settings','Calculation Overlay'):
                    overlayString = overlayString + "\nX center: " + str(round(comX,1))
                    overlayString = overlayString + "\nY center: " + str(round(comY,1))
                    #overlayString = overlayString + "\nPixel Sum: " + str(totalLight)
                self.text.set_property("text",overlayString)


            #Check to see if auto exposure channel is on or off
            try:
                newAutoExposure = int(autoExposureChannelPV.get())
            except TypeError:
                self.logger.error("failed to read: " + baseChannelName + '_AUTO')
                autoExposureChannelPV.disconnect()
                autoExposureChannelPV = PV(baseChannelName+'_AUTO')
            else:
                if newAutoExposure == 1:
                    self.config.set('Camera Settings','Auto Exposure','True')
                else:
                    self.config.set('Camera Settings','Auto Exposure','False')

            #Are we auto-exposing?
            if not self.config.getboolean('Camera Settings','Auto Exposure'):
                #If not, read exposure channel and use it
                try:
                    newExposure = int(exposureChannelPV.get())
                except TypeError:
                    self.logger.error("failed to read: " + baseChannelName + '_EXP')
                    #Try and reconnect the process variable
                    exposureChannelPV.disconnect()
                    exposureChannelPV = PV(baseChannelName+'_EXP')
                else:
                    if newExposure != self.config.getint('Camera Settings','Exposure'):
                        self.source.set_property("exposure",newExposure)
                        self.logger.info('Manually setting exposure to ' + str(newExposure))
                        self.config.set('Camera Settings','Exposure', str(newExposure))
            else:
                #Ensure the exposure epics channel is up to date
                try:
                    oldExposure = int(exposureChannelPV.get())
                except TypeError:
                    self.logger.error("failed to read: " + baseChannelName + '_EXP')
                    #Try and reconnect the process variable
                    exposureChannelPV.disconnect()
                    exposureChannelPV = PV(baseChannelName+'_EXP')
                else:
                    if oldExposure != self.config.getint('Camera Settings','Exposure'):
                        exposureChannelPV.put(self.config.getint('Camera Settings','Exposure'))

            #Are we supposed to take a snap shot and save it?
            takeSnapshot = 0
            try:
                takeSnapshot = int(snapshotChannelPV.get())
            except TypeError:
                self.logger.error("failed to read: " + baseChannelName+'_SNAP')
                #Try and reconnect the process variable
                snapshotChannelPV.disconnect()
                snapshotChannelPV = PV(baseChannelName+'_SNAP')
            else:
                if takeSnapshot:
                    if self.config.get('Camera Settings','Frame Type') == 'Mono12':
                        snapShot = Image.new('I',[self.config.getint('No Reload Camera Settings','width'),self.config.getint('No Reload Camera Settings','height')])
                    else:
                        snapShot = Image.new('L',[self.config.getint('No Reload Camera Settings','width'),self.config.getint('No Reload Camera Settings','height')])
                    snapShot.putdata(list(localNumpyArray.flat))
                    snapFileName = snapFileNameChannelPV.get()
                    if snapFileName is None:
                        snapFileNameChannelPV.disconnect()
                        snapFileNameChannelPV = PV(baseChannelName+'_FILE')
                    if snapFileName == '':
                        finalSnapSaveName = ''.join([self.config.get('Camera Settings','Snapshot Directory Path').rstrip(),'/',self.config.get('Camera Settings','Camera Name'),'_',time.strftime('%Y-%m-%d-%H-%M-%S')])
                    else:
                        finalSnapSaveName = ''.join([self.config.get('Camera Settings','Snapshot Directory Path').rstrip(),'/',snapFileName,'_',self.config.get('Camera Settings','Camera Name'),'_',time.strftime('%Y-%m-%d-%H-%M-%S')])
                    if self.config.getint('Camera Settings','Number of Snapshots') > 1:
                        finalSnapSaveName = finalSnapSaveName + '_' + str(self.snapShotCount) + '.tiff'
                    else:
                        finalSnapSaveName = finalSnapSaveName + '.tiff'
                    self.logger.info("Taking snapshot: " + finalSnapSaveName)
                    try:
                        snapShot.save(finalSnapSaveName,"TIFF")
                    except IOError:
                        self.logger.warning("Error writing snapshot file to " + str(finalSnapSaveName))
                        snapshotChannelPV.put(0)
                        snapFileName.put('ERROR WRITING SNAPSHOT')
                    else:
                        if self.snapShotCount < self.config.getint('Camera Settings','Number of Snapshots'):
                            self.snapShotCount = self.snapShotCount + 1
                        else:
                            snapshotChannelPV.put(0)
                            self.snapShotCount = 1

            #Are we reseting the archival image timer and/or changing the archival time?
            newArchiveTime = self.config.getint('Camera Settings','Archive Image Minute Interval')
            try:
                newArchiveTime = int(archiveTimePV.get()) 
            except TypeError:
                self.logger.warning("failed to read: " + baseChannelName + '_ARCHIVE_INTERVAL')
                archiveTimePV.disconnect()
                archiveTimePV = PV(baseChannelName+'_ARCHIVE_INTERVAL')
            else:
                if newArchiveTime != self.config.getint('Camera Settings','Archive Image Minute Interval'):
                    self.config.set('Camera Settings','Archive Image Minute Interval',str(int(newArchiveTime)))
                    self.logger.info('Changed archival image minuate interval to ' + self.config.get('Camera Settings','Archive Image Minute Interval'))
                archiveReset = 0
                try:
                    archiveReset = int(archiveResetPV.get())
                except TypeError:
                    self.logger.warning("failed to read: " + baseChannelName + '_ARCHIVE_RESET')
                    archiveResetPV.disconnect()
                    archiveResetPV = PV(baseChannelName+'_ARCHIVE_RESET')
                else:
                    if archiveReset == 1:
                        self.archiveTime = time.time() - self.config.getint('Camera Settings','Archive Image Minute Interval')*60 - 1
                        archiveResetPV.put(0)
                        self.logger.info('Reset archival image counter')

            #Are we supposed to take an archival image and save it?
            if int(self.config.getint('Camera Settings','Archive Image Minute Interval')) >= 1:
                if (time.time() - self.archiveTime) > 60 * int(self.config.getfloat('Camera Settings','Archive Image Minute Interval')):
                    temp_archive_directory = self.config.get('Camera Settings','Archive Image Directory') + time.strftime('%Y/%m/%d/')
                    try:
                        #If fails due to directory already exists, try to continue
                        os.makedirs(temp_archive_directory)
                    except OSError:
                        pass
                    if self.config.get('Camera Settings','Frame Type') == 'Mono12':
                        snapShot = Image.new('I',[self.config.getint('No Reload Camera Settings','width'),self.config.getint('No Reload Camera Settings','height')])
                    else:
                        snapShot = Image.new('L',[self.config.getint('No Reload Camera Settings','width'),self.config.getint('No Reload Camera Settings','height')])
                    snapShot.putdata(list(localNumpyArray.flat))
                    try:
                        archiveSnapSaveName = temp_archive_directory + self.config.get('Camera Settings','Camera Name') + '_' + time.strftime('%Y-%m-%d-%H-%M-%S') + '.tiff'
                        self.logger.info("Taking archival snapshot")
                        snapShot.save(archiveSnapSaveName,"TIFF")
                        self.archiveTime = time.time()
                    except IOError:
                        self.logger.warning("failed to write archival tiff " + str(archiveSnapSaveName))
                        self.archiveTime = time.time()
                    except TypeError:
                        self.logger.error("type error on archive snapshot name")
                        self.archiveTime = time.time()


            #Are we supposed to reload the configuration file?
            reloadChannel = 0
            try:
                reloadChannel = int(reloadChannelPV.get())
            except TypeError:
                self.logger.warning("failed to read: " + baseChannelName + '_RELOAD')
                reloadChannelPV.disconnect()
                reloadChannelPV = PV(baseChannelName+'_RELOAD')
            else:
                if reloadChannel:
                    if os.path.isfile(self.config.get('Camera Settings','iniFile')):
                        self.logger.info("Reloading settings from " + str(self.config.get('Camera Settings','iniFile')))
                        self.callingThread.loadSettings(self.config.get('Camera Settings','iniFile'))
                        self.callingThread.updateProperties()
                    else:
                        self.logger.error("Could not find ini file to reload")
                    reloadChannelPV.put(0)

            #We have no new data to process from the source element, so we just pass on the rest of this loop
            #
            if self.handOffFlag[0] != 0:
                continue

            localNumpyArray = copy.deepcopy(self.numpyArray[0])
            #We have new data, proceed to make calculations based off of it
            #Auto exposure loop
            if (self.config.getboolean('Camera Settings','Auto Exposure') == True):
                if self.config.get('Camera Settings','Frame Type') == 'Mono12':
                    maxPixelValue = 4095
                else:
                    maxPixelValue = 255
                try:
                    if numpy.max(localNumpyArray) < self.config.getint('Camera Settings','Auto Exposure Minimum'):
                        self.config.set('Camera Settings','exposure',str(int(self.config.getint('Camera Settings','exposure')*maxPixelValue*0.8/numpy.max(localNumpyArray))))
                        newExposure = self.config.getint('Camera Settings','exposure')
                        self.source.set_property("exposure",self.config.getint('Camera Settings','exposure'))
                        exposureChannelPV.put(self.config.getfloat('Camera Settings','exposure'))
                        self.logger.info("Auto raising exposure to " + self.config.get('Camera Settings','exposure'))
                    elif numpy.max(localNumpyArray) == maxPixelValue:
                        self.config.set('Camera Settings', 'exposure', str(self.config.getint('Camera Settings','exposure')/4))
                        newExposure = self.config.getint('Camera Settings','exposure')
                        self.source.set_property("exposure",self.config.getint('Camera Settings','exposure'))
                        exposureChannelPV.put(self.config.getfloat('Camera Settings','exposure'))
                    else:
                        #Force external EPICS exposure channel to match internal exposure value in case someone has changed it
                        exposureChannelPV.put(self.config.getfloat('Camera Settings','exposure'))
                except:
                    pass

            #The following calculations are optional and can be turned on and off from the configuration file
            if self.config.getboolean('Camera Settings','Calculations'):
                #Test communications to the PVs
                if comXChannelPV.get() is None:
                    self.logger.warning("failed to read or write: " + baseChannelName+'_X')
                    comXChannelPV.disconnect()
                    comXChannelPV = PV(baseChannelName+'_X')

                if comYChannelPV.get() is None:
                    self.logger.warning("failed to read or write: " + baseChannelName+'_Y')
                    comYChannelPV.disconnect()
                    comYChannelPV = PV(baseChannelName+'_Y')

                if varXChannelPV.get() is None:
                    self.logger.warning("failed to read or write: " + baseChannelName+'_WX')
                    varXChannelPV.disconnect()
                    varXChannelPV = PV(baseChannelName+'_WX')

                if varYChannelPV.get() is None:
                    self.logger.warning("failed to read or write: " + baseChannelName+'_WY')
                    varYChannelPV.disconnect()
                    varYChannelPV = PV(baseChannelName+'_WY')

                if sumChannelPV.get() is None:
                    self.logger.warning("failed to read or write: " + baseChannelName+'_SUM')
                    sumChannelPV.disconnect()
                    sumChannelPV = PV(baseChannelName+'_SUM')

                comX = -1
                comY = -1
                totalLight = -1

                startTime = time.time()
                if self.config.get('Camera Settings','Calculation Type').lower() == 'basic':

                    #The second input to the greater function is the chosen floor
                    #below which we ignore values for statistical calculations
                    maskedArray = greater(localNumpyArray,self.config.getint('Camera Settings','Calculation Noise Floor'))*localNumpyArray
                    #self.saturation_count[0] = numpy.sum(greater(localNumpyArray, 254))
                    #self.underflow_count[0] = numpy.sum(less(localNumpyArray, 2))

                    self.logger.info('Masked Array time is = ' + str(time.time() - startTime))

                    #Center of Mass (centroid), Covariance Matrix computations 
                    totalLight = scipy.sum(maskedArray)

                    firstMomentXMatrix = scipy.multiply(self.xmatrix,maskedArray)
                    firstMomentYMatrix = scipy.multiply(self.ymatrix,maskedArray)

                    secondMomentXMatrix = scipy.multiply(self.xmatrix,firstMomentXMatrix)
                    secondMomentYMatrix = scipy.multiply(self.ymatrix,firstMomentYMatrix)

                    firstMomentX = scipy.sum(firstMomentXMatrix)/totalLight
                    firstMomentY = scipy.sum(firstMomentYMatrix)/totalLight

                    secondMomentX = 2*(scipy.sum(secondMomentXMatrix)/totalLight - firstMomentX**2)**0.5
                    secondMomentY = 2*(scipy.sum(secondMomentYMatrix)/totalLight - firstMomentY**2)**0.5

                    self.logger.info('Moment calc time is = ' + str(time.time() - startTime))

                elif self.config.get('Camera Settings','Calculation Type').lower() == 'squared':
                    squaredArray = scipy.square(localNumpyArray)

                    #Center of Mass (centroid), Covariance Matrix computationsi
                    totalLight = scipy.sum(localNumpyArray)
                    totalLightSquared = scipy.sum(squaredArray)

                    firstMomentXMatrix = scipy.multiply(self.xmatrix,squaredArray)
                    firstMomentYMatrix = scipy.multiply(self.ymatrix,squaredArray)

                    firstMomentX = scipy.sum(firstMomentXMatrix)/totalLightSquared
                    firstMomentY = scipy.sum(firstMomentYMatrix)/totalLightSquared

                    secondMomentXMatrix = scipy.multiply(scipy.square(self.xmatrix-firstMomentX),squaredArray)
                    secondMomentYMatrix = scipy.multiply(scipy.square(self.ymatrix-firstMomentY),squaredArray)

                    secondMomentX = scipy.sqrt(scipy.sum(secondMomentXMatrix)/totalLightSquared)
                    secondMomentY = scipy.sqrt(scipy.sum(secondMomentYMatrix)/totalLightSquared)

                    self.logger.info('Moment calc time is = ' + str(time.time() - startTime))

                #[comY,comX] =  scipy.ndimage.measurements.center_of_mass(maskedArray)
                comX = firstMomentX 
                comY = self.config.getint('No Reload Camera Settings','height') - (firstMomentY + self.config.getint('No Reload Camera Settings','Y'))

                #If we have nan calculations, need to put something real into the EPICS channels so we choose -1
                if str(comX) != 'nan':
                    comXChannelPV.put(float(comX))
                else:
                    comXChannelPV.put(float(-1))

                if str(comY) != 'nan': 
                    comYChannelPV.put(float(comY))
                else:
                    comYChannelPV.put(-1)

                if str(secondMomentX) != 'nan':
                    varXChannelPV.put(float(secondMomentX))
                else:
                    varXChannelPV.put(-1)

                if str(secondMomentY) != 'nan':
                    varYChannelPV.put(float(secondMomentY))
                else:
                    varYChannelPV.put(-1)

                #if str(stdDevXY) != 'nan':
                #    epics.caput(self.settingsDict['varXYChannel'],float(stdDevXY))
                #else:
                #    epics.caput(self.settingsDict['varXYChannel'],-1)

                if str(totalLight) != 'nan':
                    sumChannelPV.put(float(totalLight))
                else:
                    sumChannelPV.put(-1)

                self.logger.info('Final calc time is = ' + str(time.time() - startTime))
            #We have finished
            self.handOffFlag[0] = 1

            self.logger.info('Hand off time is = ' + str(time.time() - startTime))
#End CommunicationThread Run functiondefinition


#---------------------------------------------------------------------
# Main
class Main:
    def __init__(self):
        # loop start time
        self.startTime = 0

        # Don't fork into the background by default
        self.daemonize = False
        self.pidFile = None

        self.logger = None
        self.time = time.time()
        self.numpyArray = [numpy.array([-1])]
        self.handOffFlag = [1]
        self.playmode = [True]

        #Default settings
        self.config = ConfigParser.ConfigParser()
        self.config.set('DEFAULT','iniFile','cameraSnapPyCam.ini')
        self.config.set('DEFAULT','Log File Directory',None)
        self.config.set('DEFAULT','Log File Level',str(logging.DEBUG))
        self.config.set('DEFAULT','width','640')
        self.config.set('DEFAULT','height','480')
        self.config.set('DEFAULT','X','0')
        self.config.set('DEFAULT','Y','0')
        self.config.set('DEFAULT','exposure','50000')
        self.config.set('DEFAULT','Analog Gain','100')
        self.config.set('DEFAULT','Auto Exposure Minimum','150')
        self.config.set('DEFAULT','Camera Name','None')
        self.config.set('DEFAULT','Base Channel Name','None')
        self.config.set('DEFAULT','Snapshot Directory Path','None')
        self.config.set('DEFAULT','Camera IP','None')
        self.config.set('DEFAULT','Name Overlay','False')
        self.config.set('DEFAULT','Time Overlay','False')
        self.config.set('DEFAULT','Calculation Overlay','False')
        self.config.set('DEFAULT','Auto Exposure','False')
        self.config.set('DEFAULT','Calculation Noise Floor','0')
        self.config.set('DEFAULT','Calculations','True')
        self.config.set('DEFAULT','Calculation Type','basic')
        self.config.set('DEFAULT','Number of Snapshots','1')
        self.config.set('DEFAULT','Black Level','64')
        self.config.set('DEFAULT','Frame Type','Mono8')
        self.config.set('DEFAULT','Archive Image Minute Interval','60')
        self.config.set('DEFAULT','Archive Image Directory','/data/camera_images/camera_archive/')
        self.config.add_section('Camera Settings')
	self.config.add_section('No Reload Camera Settings')

        xones = scipy.ones([self.config.getint('No Reload Camera Settings','height'),1])
        xran = scipy.arange(self.config.getint('No Reload Camera Settings','X'),self.config.getint('No Reload Camera Settings','width')).reshape(1,self.config.getint('No Reload Camera Settings','width') - self.config.getint('No Reload Camera Settings','X'))
        self.xmatrix = scipy.dot(xones,xran)
        yones = scipy.ones([1,self.config.getint('No Reload Camera Settings','width')])
        yran = scipy.arange(self.config.getint('No Reload Camera Settings','Y'),self.config.getint('No Reload Camera Settings','height')).reshape(self.config.getint('No Reload Camera Settings','height') - self.config.getint('No Reload Camera Settings','Y'),1)
        self.ymatrix = scipy.dot(yran,yones)


        # parse command line
        try:
            opts,args = getopt.getopt(sys.argv[1:],"hu:c:p:l:d",["help","uid=","config=","pidfile=","logfiledir=","daemon"])
        except getopt.GetoptError:
            usage()
            sys.exit(2)

        for o, a in opts:
            if o in ("-h", "--help"):
                usage()
                sys.exit()
            elif o in ("-d", "--daemon"):
                self.daemonize = True
            elif o in ("-p", "--pidfile"):
                self.pidFile = str(a)
            elif o in ("-l", "--logfiledir"):
                self.config.set('Camera Settings','Log File Directory',str(a))
            elif o in ("-c", "--config"):
                self.config.set('Camera Settings','iniFile',str(a))
                if not os.path.isfile(self.config.get('Camera Settings','iniFile')):
                    sys.stderr.write('Could not find file \'' + self.config.get('Camera Settings','iniFile') + '\'\n')
                    sys.exit(1)
            else:
                assert False, "unhandled option"


        #Setup main logger
        try:
            if (self.config.get('Camera Settings','Log File Directory') != None) and (os.path.isdir(self.config.get('Camera Settings','Log File Directory'))):
                logFileName = self.config.get('Camera Settings','Log File Directory').strip() + '/' + os.path.basename(self.config.get('Camera Settings','iniFile').rstrip('.ini')) + '_main.log'
                self.logger = logging.getLogger('main')
                self.logger.setLevel(self.config.getint('Camera Settings','Log File Level'))
                handler = logging.handlers.TimedRotatingFileHandler(logFileName,when='W6',delay=True)
                handler.setLevel(self.config.getint('Camera Settings','Log File Level'))
                formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
                handler.setFormatter(formatter)
                self.logger.addHandler(handler)
            else:
                sys.stderr.write('Could not find log directory \'' + self.config.get('Camera Settings','Log File Directory') + '\'\n')
                self.logger = logging.getLogger('main')
                self.logger.setLevel(self.config.getint('Camera Settings','Log File Level'))
                handler = logging.StreamHandler()
                handler.setLevel(self.config.getint('Camera Settings','Log File Level'))
                formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
                handler.setFormatter(formatter)
                self.logger.addHandler(handler)

        except:
            self.logger = logging.getLogger('main')
            self.logger.setLevel(self.config.getint('Camera Settings','Log File Level'))
            handler = logging.StreamHandler()
            handler.setLevel(self.config.getint('Camera Settings','Log File Level'))
            formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
            handler.setFormatter(formatter)
            self.logger.addHandler(handler)


        if self.daemonize:
            if (os.fork()): sys.exit()


        if self.pidFile:
            try:
                pidFileHandle = open(self.pidFile,'w')
                pidFileHandle.write(str(os.getpid()))
                pidFileHandle.close()
            except IOError, e:
                print "Could not write to pid file %s\n" % (self.pidFile)
                print e
                sys.exit(2)


        # load settings
        if os.path.isfile(self.config.get('Camera Settings','iniFile')):
            self.logger.info("loading settings from " + self.config.get('Camera Settings','iniFile'))
            self.loadSettings(self.config.get('Camera Settings','iniFile'))
            self.logger.info("finished loading settings from " + self.config.get('Camera Settings','iniFile'))
            self.logger.debug("settings are:\n" + str(self.config._sections))

        # Gstreamer setup section
        # initialize pipeline
        self.player = gst.Pipeline("player")
        self.source = pcamerasrc.PCameraSource("source")

        # set parameters
        assert self.source
        self.updateProperties()

        #Handles conversion to different video capabilities
        colorspace = gst.element_factory_make("ffmpegcolorspace","colorspace")

        #Text overlay element
        self.text = gst.element_factory_make("textoverlay","text0")
        self.text.set_property("valignment",2)
        self.text.set_property("halignment",0)
        if self.config.getboolean('Camera Settings','Name Overlay'):
            self.text.set_property("text",self.config.get('Camera Settings','Camera Name'))
        else:
            self.text.set_property("text",'')

        #Queue enables threadings, so the encoder/tcp server is in a seperate thread
        queue = gst.element_factory_make("queue","frameQueue")

        #Encodes into jpegs - switching this element out for a different encoder require you to
        #also switch in the client code for the correct decoder
        #encoder = gst.element_factory_make("jpegenc","encoder0")
        encoder = gst.element_factory_make("x264enc","encoder0")
        encoder.set_property("pass", "qual")
        encoder.set_property("quantizer", 20)
        encoder.set_property("tune", "zerolatency")
        rtpencoder = gst.element_factory_make("rtph264pay","rtpenc0")
        caps_string = 'video/x-raw-yuv,width=' + self.config.get('No Reload Camera Settings','width') + ',height=' + self.config.get('No Reload Camera Settings','height')
        caps = gst.Caps(caps_string)
        capsfilter = gst.element_factory_make("capsfilter", "filter")
        capsfilter.set_property("caps", caps)

        #Handles tcp communication
        udpserversnk = gst.element_factory_make("udpsink",'udpsink0')
        udpserversnk.set_property("host",self.config.get('No Reload Camera Settings','Multicast Group'))
        udpserversnk.set_property("port",self.config.getint('No Reload Camera Settings','Multicast Port'))
        #tcpserversnk = gst.element_factory_make("tcpserversink",'tcpserversnk0')
        #tcpserversnk.set_property("host",'127.0.0.1')
        #tcpserversnk.set_property("port",self.TCPport)

        #Puts everything in the Pipeline "player"
        #self.player.add(self.source,colorspace,queue,self.text,encoder,tcpserversnk)
        #gst.element_link_many(self.source,colorspace,queue,self.text,encoder,tcpserversnk)
        self.player.add(self.source,colorspace,queue,self.text,encoder,capsfilter,rtpencoder,udpserversnk)
        gst.element_link_many(self.source,colorspace,queue,self.text,capsfilter,encoder,rtpencoder,udpserversnk)

        #End Gstreamer setup section

        # start communications and image fitting thread
        self.commAndFit = CommunicationThread(self)
        self.commAndFit.daemon = True
        self.commAndFit.start()

    #End Main initialization

    #----------------------------------------
    def updateProperties(self):
        self.source.set_property("IP",self.config.get('No Reload Camera Settings','Camera IP'))
        self.source.set_property("exposure",self.config.getint('Camera Settings','exposure'))
        self.source.set_property("gain",self.config.getint('Camera Settings','Analog Gain'))
        self.source.set_property("NumpyArrayReference",self.numpyArray)
        self.source.set_property("NumpyArrayHandOff",self.handOffFlag)
        self.source.set_property("X",self.config.getint('No Reload Camera Settings','X'))
        self.source.set_property("Y",self.config.getint('No Reload Camera Settings','Y'))
        self.source.set_property("height",self.config.getint('No Reload Camera Settings','height'))
        self.source.set_property("width",self.config.getint('No Reload Camera Settings','width'))
        self.source.set_property("startTime",self.startTime)
        self.source.set_property("blackLevel",self.config.getint('Camera Settings','Black Level'))
        if self.config.get('Camera Settings','Log File Directory'): 
            logFileName = self.config.get('Camera Settings','Log File Directory').strip() + '/' + os.path.basename(self.config.get('Camera Settings','iniFile').rstrip('.ini')) + '_gst.log'
        else:
            logFileName = None
        self.source.set_property("logFileName",logFileName)
        self.source.set_property("frameType",self.config.get('Camera Settings','Frame Type'))

    #-----------------------------------------------------------------

    def loadSettings(self,fileName):
        try:
            self.config.read(fileName)

            #Generate scipy arrays for calculations of center of mass, higher order moments
            xones = scipy.ones([self.config.getint('No Reload Camera Settings','height'),1])
            xran = scipy.arange(self.config.getint('No Reload Camera Settings','X'),self.config.getint('No Reload Camera Settings','width')).reshape(1,self.config.getint('No Reload Camera Settings','width') - self.config.getint('No Reload Camera Settings','X'))
            self.xmatrix = scipy.dot(xones,xran)
            yones = scipy.ones([1,self.config.getint('No Reload Camera Settings','width')])
            yran = scipy.arange(self.config.getint('No Reload Camera Settings','Y'),self.config.getint('No Reload Camera Settings','height')).reshape(self.config.getint('No Reload Camera Settings','height') - self.config.getint('No Reload Camera Settings','Y'),1)
            self.ymatrix = scipy.dot(yran,yones)
            return True
        except IOError:
            self.logger.error('Error opening or reading from config file: ' + fileName)
            return False
        except ValueError:
            self.logger.error('Syntax error in config file: ' + fileName)
            self.logger.error('Did not understand: ' + line)
            return False
        if self.config.getboolean('Camera Settings','Auto Exposure'):
            autoExposureChannelPV.put(0)
        else:
            autoExposureChannelPV.put(1)
        self.logger.debug("settings are:\n " + str(self.config._sections))

    #-----------------------------------------------------------------

    # main loop
    def start(self):
        self.playmode[0] = True
        self.player.set_state(gst.STATE_PLAYING)
        while self.playmode[0]:
            time.sleep(1)
        loop.quit()

    #----------------------------------------------------------------

    def stop(self, signum, frame):
        self.player.set_state(gst.STATE_NULL)
        self.playmode[0] = False


#----------------------------------------------------------
# MAIN
        
# start Gstreamer pipeline setup
print "initializing..."
cam1 = Main()

#Handles kill signals
signal.signal(signal.SIGTERM, cam1.stop)

# start server
print "starting server..."
thread.start_new_thread(cam1.start, ())
loop = gobject.MainLoop()
loop.run()
