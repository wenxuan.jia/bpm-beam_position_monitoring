function [sx, sy, newcurrsg] = getScatterMotion(prevsg, currsg)
    % brute force
%     deltax = -2:.5:2;
%     deltay = -2:.5:2;
% 
%     [eyg, ~] = meshgrid(deltay, deltax);
%     err = zeros(size(eyg));
% 
% 
%     for i = 1:length(deltax)
%         for j = 1:length(deltay)
%             err(i,j) = sge_fun([0 0 deltax(i); 0 0 deltay(j)], sg0, sg1);
% 
%         end
%     end
%     
%     figure; surf(deltay, deltax, err); shading interp; view([0 90]); colormap('jet');
%     xlabel('Y error'); ylabel('X error')
%     return

    
    sx = 0; sy = 0;
    derrd = [NaN, NaN];
    posstep = 0.1;
    
    damping = 0;  % it's in [0,1]. 0 is no damping.
    
    tol = 0.1;
    
    stepsizex = 0.005; stepsizey = stepsizex;
    i = 0;
    accumu = 0;
    stepmoved = Inf;
    
%     figure 
%     h = animatedline; 
%     axis([x0-3, x0+3, y0-3, y0+3]) 
    
    
    while stepsizex + stepsizey > 0 % pixel unit
        if i > 1
            sx(i+1) = sx(i) - stepsizex*( (1)*derrd(i,1) + damping*derrd(i-1,1) );
            sy(i+1) = sy(i) - stepsizey*( (1)*derrd(i,2) + damping*derrd(i-1,2) );
        elseif i == 1
            sx(i+1) = sx(i) - stepsizex*derrd(i,1);
            sy(i+1) = sy(i) - stepsizey*derrd(i,2);
        end
        i = i+1; accumu = accumu + 1;
        derrd(i,1) = sge_fun([0 0 sx(i)+posstep; 0 0 sy(i)], prevsg, currsg) - sge_fun([0 0 sx(i); 0 0 sy(i)], prevsg, currsg);;
        derrd(i,2) = sge_fun([0 0 sx(i); 0 0 sy(i)+posstep], prevsg, currsg) - sge_fun([0 0 sx(i); 0 0 sy(i)], prevsg, currsg);
        derrd(i,:) = derrd(i,:)/posstep;
        
        if i == 1
            stepmovedx = stepsizex*derrd(i,1);
            stepmovedy = stepsizey*derrd(i,2);
        else
            stepmovedx = sx(i) - sx(i-1);
            stepmovedy = sy(i) - sy(i-1);
        end
        
        if abs(stepmovedx) < tol
            stepsizex = 0;
        end
        
        if abs(stepmovedy) < tol
            stepsizey = 0;
        end
        
        
        if accumu > 20
            damping = damping + 0.1;
            stepsizex = stepsizex * 0.7;
            stepsizey = stepsizey * 0.7;
            
            accumu = 0;
        end
        
%         addpoints(h,xgd(i),ygd(i)) 
%         drawnow limitrate nocallbacks
        
    end
    
    
    
    
    
    
    
    
    
    % make new scatter map grid
    [xlen, ylen] = size(prevsg);
    [yg, xg] = meshgrid(1:ylen, 1:xlen);
    
    newxg = xg; newyg = yg;
    affc = [0 0 sx(end); 0 0 sy(end); 0 0 0];
    affc = affc + eye(3,3);
    
    for i = 1:xlen
        for j = 1:ylen
            newv = affc*[xg(i,j); yg(i,j); 1];
            
            newxg(i,j) = newv(1);
            newyg(i,j) = newv(2);
        end
    end
    
    newcurrsg = interp2(yg, xg, prevsg, newyg, newxg);

    sx = sx(end); sy = sy(end);

end