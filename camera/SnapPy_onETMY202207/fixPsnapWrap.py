#!/usr/bin/env python

import os, sys

wrap_file = open('psnap_wrap.cxx','r')

wrap_lines = wrap_file.readlines()

wrap_file.close()

os.remove('psnap_wrap.cxx')

wrap_file = open('psnap_wrap.cxx','w')

print 'Removing the following lines:'

skip = False
for line in wrap_lines:
  if 'SWIGINTERN PyObject *_wrap_CameraStruct_Camera_set(PyObject *SWIGUNUSEDPARM(self), PyObject *args) {' in line:
    skip = True
  if 'SWIGINTERN PyObject *_wrap_new_CameraStruct(PyObject *SWIGUNUSEDPARM(self), PyObject *args) {' in line:
    skip = False
  if '{ (char *)"CameraStruct_Camera_set", _wrap_CameraStruct_Camera_set, METH_VARARGS, NULL},' in line:
    skip = True
  if '{ (char *)"new_CameraStruct", _wrap_new_CameraStruct, METH_VARARGS, NULL},' in line:
    skip = False
  if not skip:
    wrap_file.write(line)
  if skip:
    sys.stdout.write(line)

wrap_file.close()

py_file = open('psnap.py','r')

py_lines = py_file.readlines()

py_file.close()

os.remove('psnap.py')

py_file = open('psnap.py','w')

skip = False
for line in py_lines:
  if '__swig_setmethods__["Camera"] = _psnap.CameraStruct_Camera_set' in line:
    skip = True
  if ' def __init__(self):' in line:
    skip = False
  if not skip:
    py_file.write(line)
  if skip:
    sys.stdout.write(line)
py_file.close()
