#!/usr/bin/env python

import epics
from epics import PV


camera_list = ['AS','BS','ETMX','ETMY','IM4_TRANS','ITMX','ITMY','MC_REFL','MC_TRANS','MC1','MC2','MC3','MC1_LAV','MC2_LAV','MC3_LAV','OMC_REFL','OMC_TRANS','PR2','PR3','PRM','PRM_REFL_180','PRM_REFL_90','PSL','SPARE1','TEMP1','TEMP2','X_IR_TRANS','Y_IR_TRANS','X_GRN_TRANS','Y_GRN_TRANS','MC3_LAV','PRM_LAV','PR2_LAV','PR3_LAV']

channel_type_list = ['_AUTO','_EXP','_RELOAD','_FILE','_SNAP','_X','_Y','_WX','_WY','_SUM','_ARCHIVE_RESET','_ARCHIVE_INTERVAL']

PVlist = []
index = 0
for camera in camera_list:
  PVlist.append([])
  for channel_type in channel_type_list:
    PVlist[index].append(PV('L1:CAM-' + camera + channel_type,None,'native',None,None,1))
  index = index + 1

for runs in range(10):
    for index in range(len(PVlist)):
        for channel in range(len(PVlist[index])):
            PVlist[index][channel].status
            PVlist[index][channel].get()
            if not PVlist[index][channel].status:
                print 'camera = ' + camera_list[index] + ' and channel = ' + channel_type_list[channel]
                print PVlist[index][channel].status
                print PVlist[index][channel].get()
