#!/usr/bin/env python

import sys
import os
import getopt
import ConfigParser

def usage():
    print "-h, --help          Displays this message"
    print "-c, --config        Uses the settings in the config file listed"


#Default
host = "127.0.0.1"
port = "5000"


#Parse command line options
try:
   opts,args = getopt.getopt(sys.argv[1:],"hc:",["help","config="])
except getopt.GetoptError:
   usage()
   sys.exit(2)

if len(opts) == 0:
    usage()
    sys.exit()

for o, a in opts:

    if o in ("-h", "--help"):
        usage()
        sys.exit()
    elif o in ("-c", "--config"):
        configFile = str(a)
    else:
        usage()
        sys.exit()

#Create config parser and grab necessary configuration information
config = ConfigParser.ConfigParser()
config.read(configFile)
port = config.get('No Reload Camera Settings','Multicast Port')
multicastGroup = config.get('No Reload Camera Settings','Multicast Group')

Done = False
path = ""
file_name = ""
user_file = ""
while not Done:
    user_file = raw_input("\nEnter full path and file name to store avi file:\n").strip()
    path, file_name = os.path.split(user_file)
    if os.path.exists(path):
        if os.path.exists(user_file):
            already_exists_continue = raw_input("The file " + user_file + " already exists. Overwrite? y/N\n").lower()
            if 'y' == already_exists_continue or 'yes' == already_exists_continue:
                Done = True
        else:
            Done = True
    else:
        print "Path does not exist.\n"

print "\nTo stop recording, use ctrl-c\n\n"
commandString = ''.join(["gst-launch udpsrc multicast-group=",str(multicastGroup)," port=",str(port), " ! application/x-rtp, payload=127 ! rtph264depay ! ffdec_h264 ! ffmpegcolorspace ! ffenc_mpeg4 ! avimux ! filesink location=",str(user_file)])
os.system(commandString)
