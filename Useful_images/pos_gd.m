function [finerx, finery] = pos_gd(fgoversg, x0, y0, I0, w)
    fgoversg(isinf(fgoversg)) = NaN;

%      % brute force
%     deltax = -4:.5:4;
%     deltay = -4:.5:4;
% 
%     [eyg, ~] = meshgrid(deltay, deltax);
%     err = zeros(size(eyg));
% 
% 
%     for i = 1:length(deltax)
%         for j = 1:length(deltay)
%             err(i,j) = gausse_fun(x0+deltax(i), y0+deltay(j), fgoversg, I0, w);
% 
%         end
%     end
%     
%     figure; surf(deltay, deltax, err); shading interp; view([0 90]); colormap('jet');
%     xlabel('Y error'); ylabel('X error')
%     return



    xgd = x0;
    ygd = y0;
    derrd = [NaN, NaN];
    
    posstep = 0.1;
    
    dampingx = 0; dampingy = 0; % it's in [0,1]. 0 is no damping.
    
    tol = 0.0025; % how flat is flat? It's flat when the oscillation begins
    
    stepsizex = 0.005; stepsizey = stepsizex;
    i = 0;
    oscillax = 0; oscillay = 0;
    
%     figure 
%     h = animatedline; 
%     axis([x0-3, x0+3, y0-3, y0+3]) 
    
    
    while stepsizex + stepsizey > 0 % pixel unit
        if i > 1
            xgd(i+1) = xgd(i) - stepsizex*( (1)*derrd(i,1) + dampingx*derrd(i-1,1) );
            ygd(i+1) = ygd(i) - stepsizey*( (1)*derrd(i,2) + dampingy*derrd(i-1,2) );
        elseif i == 1
            xgd(i+1) = xgd(i) - stepsizex*derrd(i,1);
            ygd(i+1) = ygd(i) - stepsizey*derrd(i,2);
        end
        i = i+1; 
        derrd(i,1) = gausse_fun(xgd(i)+posstep, ygd(i), fgoversg, I0, w) - gausse_fun(xgd(i), ygd(i), fgoversg, I0, w);
        derrd(i,2) = gausse_fun(xgd(i), ygd(i)+posstep, fgoversg, I0, w) - gausse_fun(xgd(i), ygd(i), fgoversg, I0, w);
        derrd(i,:) = derrd(i,:)/posstep;
        
        if i == 1
            stepmovedx = stepsizex*derrd(i,1);
            stepmovedy = stepsizey*derrd(i,2);
        else
            stepmovedx = xgd(i) - xgd(i-1);
            stepmovedy = ygd(i) - ygd(i-1);
        end
        
        if abs(stepmovedx) < tol && oscillax
            stepsizex = 0;
        end
        
        if abs(stepmovedy) < tol && oscillay
            stepsizey = 0;
        end
        
        if i > 3 && derrd(i,1)*derrd(i-1,1) < 0
            oscillax = oscillax + 1;
        end
        
        if i > 3 && derrd(i,2)*derrd(i-1,2) < 0
            oscillay = oscillay + 1;
        end
        
        
        if ~oscillax
            stepsizex = stepsizex * 1.4;
        elseif oscillax > 10
            dampingx = dampingx + 0.1;
            stepsizex = stepsizex * 0.7;
            oscillax = 1;
        end
        
        if ~oscillay
            stepsizey = stepsizey * 1.4;
        elseif oscillay > 10
            dampingy = dampingy + 0.1;
            stepsizey = stepsizey * 0.7;
            oscillay = 1;
        end
        
%         addpoints(h,xgd(i),ygd(i)) 
%         drawnow limitrate nocallbacks
        
    end
    
    finerx = xgd(end);
    finery = ygd(end);
    
end