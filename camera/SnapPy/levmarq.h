#pragma once
#include <math.h>
#include <string.h>

#include <string>
#include <vector>
#include <stdio.h>
#include <algorithm>

using namespace std;

typedef struct {
	int verbose; // print every iteration
	int max_it; // max iteration allowed
	double init_lambda;
	double up_factor;
	double down_factor;
	double target_derr;
	int final_it; // final iterations used
	double final_err;
	double final_derr;
} LMstat;

//void levmarq_init(LMstat* lmstat);

int levmarq(int npar, double* par, int ny, double* y, double* dysq, double LMgradientDelta,
	double (*func)(double*, int, void*),
	void (*grad)(double*, double*, int, void*, double),
	void* fdata, LMstat* lmstat);

double error_func(double* par, int ny, double* y, double* dysq,
	double (*func)(double*, int, void*), void* fdata);

void solve_axb_cholesky(int n, double** l, double* x, double* b);

int cholesky_decomp(int n, double** l, double** a);


//double LMfunc_calcI(double* paramToFit, int i, double* rlist); // LM func. paramToFit = [I0, w]
//void LMgrad_calcI(double* gradient, double* paramToFit, int i, double* rlist); // LM grad. paramToFit = [I0, w]
