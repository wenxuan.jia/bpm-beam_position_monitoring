function avi2gif(varargin) % output is double instead of uint8
    for i = 1:2:length(varargin)
        temp = varargin{i+1};
        eval([varargin{i} '= temp;'])
    end
    
    if ~exist('avg', 'var')
        avg = 25; % default fps. Output frequency = fps/avg
    end
    
    if ~exist('frame', 'var')
        error('\n\n Please specify frames to input!!!\n\n');
    end
    
    
%     name = ['*2018-10-31*'];

    %search file with the name specified
    files = dir(['./', name]);
    %search subfolders
    files = [files; dir(['**/', name])];
    
    
    i = 1;
    if(isempty(files))
        error('\n\n No data found with filename ''%s''!!!\n\n', name);

    elseif(length(files) > 1)
        fprintf('\n More than one data found with filename ''%s'':\n', name);
        fprintf('    0: Load all of the following:\n')
        for j = 1:length(files)
            fprintf('    %d: %s\n', j, files(j).name);
        end
        fprintf('\n');
        i = input(' Please choose the one by index: ');

    end
        
    if i == 0
%         filename = {};
%         for j = 1:length(files)
%             fileadr = [files(j).folder, '/', files(j).name];
%             filename = [filename; files(j).name];
%             rawimg = [rawimg; imread(fileadr)];
%         end
        
    else
        
        fileadr = [files(i).folder, '/', files(i).name];
        filename = files(i).name;
        videoReader = VideoReader(fileadr);
        frames = read(videoReader, frame);
        [~, ~, ~, framenum] = size(frames);
        
        RGB = 1; % the RGB layers are identical, so pick whatever one
        imwrite(frames(:,:,RGB,1:5:end),[files(i).folder '/' 'tmp.gif'],'gif','LoopCount',Inf,'DelayTime',1/5);
        
        
%         for j = 0:floor(framenum/avg)-1
%             imgsum = zeros(480, 640);
%             avgnum = 0;
%             for k = 1:avg
%                 tmp = double(frames(:,:,RGB,j*avg+k));
%                 if mode(mode(tmp)) ~= 128 % bad image
%                     imgsum = imgsum + tmp;
%                     avgnum = avgnum + 1;
%                 end
%             end
%             
%             imwrite(uint8(imgsum/avgnum), [num2str(j*avg+k) '.png']);
%         end
        
        
        
        
    end
    
    
    
    % Calibrate exposure
    
    
    % Calibrate power
    
    
    
    % Subtract the reaction mass reflection behind test mass
    
    
    
    return
    
end
