function [err, I0, w] = f1e_fun(posx, posy, img, plt) % img has NaN

    if ~exist('plt', 'var')
        plt = false;
    end
    
    [xlen, ylen] = size(img);
    [yg, xg] = meshgrid(1:ylen, 1:xlen); % image indexing is flipped


    rg = sqrt((xg-posx).^2 + (yg-posy).^2);
    rlist = rg(:);
    ilist  = double(img(:));
    
    
    rlist(isnan(ilist)) = [];
    ilist(isnan(ilist)) = [];
    ilist(rlist <= 10)  = [];
    rlist(rlist <= 10)  = [];
    
    
    myfittype = fittype(['I0.*exp(-2*r.^2./w.^2)'],...
                'dependent',{'y'},'independent',{'r'},...
                'coefficients',{'I0','w'});

    myfit = fit(rlist, ilist, myfittype, 'StartPoint', [600, 70], 'Lower', [0, 0]);
    I0 = myfit.I0;
    w = myfit.w;
    
    if plt
        figure; plot(rlist, ilist, '.'); hold on; 
        r = 0:200;
        plot(r, myfit(r))
    
    end
    
    err = sqrt(sum( (ilist - I0.*exp(-2*rlist.^2./w.^2)).^2 ));
end