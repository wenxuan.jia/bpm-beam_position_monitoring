#include <psnap.hpp>

typedef Pylon::CBaslerGigECamera Camera_t;
using namespace Basler_GigECameraParams;
using namespace Basler_GigEStreamParams;


using namespace Pylon;
using namespace std;

//********************************
// Creates and opens a camera object for the first available GigE camera, used before "openCamera"
bool getCamera(CameraStruct* myCamera)
{
  CTlFactory& TlFactory = CTlFactory::GetInstance();
  ITransportLayer* pTl = TlFactory.CreateTl( CBaslerGigECamera::DeviceClass() );

  DeviceInfoList_t lstDevices;
  pTl->EnumerateDevices( lstDevices );
  if ( lstDevices.empty() ) {
    cerr <<  "No devices found" << endl;
    flush(cerr);
    return false;
  }

  myCamera->Camera.Attach( TlFactory.CreateDevice( lstDevices[0] ) );

  return true;
}
//*********************************

//*********************************
// Creates a camera object for a GigE camera based on IP, used before "openCamera"
bool getCameraByAddr(CameraStruct* myCamera, char* IpAddress )
{
  String_t IpString;

  IpString = IpAddress;

  CTlFactory& TlFactory = CTlFactory::GetInstance();


  ITransportLayer* pTl = TlFactory.CreateTl( CBaslerGigECamera::DeviceClass() );

  DeviceInfoList_t filter;

  filter.push_back( CDeviceInfo().SetPropertyValue("IpAddress", IpString));

  DeviceInfoList_t lstDevices;
  pTl->EnumerateDevices( lstDevices,filter );
  if ( lstDevices.empty() ) {
    cerr <<  "No devices found" << endl;
    flush(cerr);
    return false;
  }

  myCamera->Camera.Attach( TlFactory.CreateDevice( lstDevices[0] ) );

  return true;
}
//*********************************

//********************************
// Opens the camera for use, used after "getCamera" or "getCameraByAddr", before "setupCamera"
int openCamera(CameraStruct* myCamera)
{
  //cerr << "Psnap.cpp opening camera" << endl;
  //flush(cerr);
  try {
    myCamera->Camera.Open();
  }
  catch ( GenICam::RuntimeException e ) {
    return 1;
  }
  return 0;
} 
//********************************

//********************************
//Closes the camera for use, used after "stopCamera"
void closeCamera(CameraStruct* myCamera) 
{
  myCamera->Camera.Close();
}
//********************************

//********************************
//Sets the parameters of the camera, used after "openCamera", before "startCamera"
bool setupCamera(CameraStruct* myCamera, int height_value, int width_value, int x_value, int y_value, int exposure_time, int analog_gain, char* PixelFormat,int black_level)
{


  String_t PixelString;

  PixelString = PixelFormat;

  int64_t min_width = myCamera->Camera.Width.GetMin();
  int64_t max_width = myCamera->Camera.Width.GetMax();

  int64_t min_height = myCamera->Camera.Height.GetMin();
  int64_t max_height = myCamera->Camera.Height.GetMax();

  int64_t min_exposure = myCamera->Camera.ExposureTimeRaw.GetMin();
  int64_t max_exposure = myCamera->Camera.ExposureTimeRaw.GetMax();

  if (x_value < 0)
  {
    x_value = min_width;    
    cerr << "X value too small.  Set to minimum of: 0" <<endl;
    flush(cerr);
  }
  if (x_value > max_width)
  {
    x_value = max_width;
    cerr << "X value too large.  Set to maximum of: " << min_width <<endl;
    flush(cerr);
  }

  if (y_value < 0)
  {
    y_value = min_height;
    cerr << "Y value too small.  Set to minimum of: 0" <<endl;
    flush(cerr);
  }

  if (y_value > max_width)
  {
    y_value = max_height;
    cerr << "Y value too large.  Set to maximum of: " << max_height <<endl;
    flush(cerr); 

  }
  myCamera->Camera.OffsetX.SetValue(0);
  myCamera->Camera.OffsetY.SetValue(0);
  myCamera->Camera.Width.SetValue(width_value);
  myCamera->Camera.Height.SetValue(height_value);
  myCamera->Camera.OffsetX.SetValue(x_value);
  myCamera->Camera.OffsetY.SetValue(y_value);


  if (exposure_time < min_exposure)
  {
    exposure_time = min_exposure;
  }
  if (exposure_time > max_exposure)
  {
    exposure_time = max_exposure;
  }

  myCamera->Camera.ExposureMode.SetValue(ExposureMode_Timed);
  myCamera->Camera.ExposureTimeRaw.SetValue(exposure_time);

  if (PixelString.compare("Mono8") == 0)
  {
    myCamera->Camera.PixelFormat.SetValue( PixelFormat_Mono8 );
  } else if (PixelString.compare("BayerBG16") == 0) {
    myCamera->Camera.PixelFormat.SetValue( PixelFormat_BayerBG16 );
  } else if (PixelString.compare("RGB8") == 0) {
    myCamera->Camera.PixelFormat.SetValue( PixelFormat_RGB8Packed );
  } else {
    myCamera->Camera.PixelFormat.SetValue( PixelFormat_Mono12 );
  }

  myCamera->Camera.GainSelector.SetValue( GainSelector_All );

  int64_t analog_gain_max = myCamera->Camera.GainRaw.GetMax();
  int64_t analog_gain_min = myCamera->Camera.GainRaw.GetMin();

  if (analog_gain < analog_gain_min )
  {
    analog_gain = analog_gain_min;
    cerr << "Analog Gain too small.  Set to minimum of: " << analog_gain_min <<endl;
    flush(cerr);
  }

  if (analog_gain > analog_gain_max )
  {
    analog_gain = analog_gain_max;
    cerr << "Analog Gain too large.  Set to maximum of: " << analog_gain_max <<endl;
    flush(cerr);
  }

  myCamera->Camera.GainRaw.SetValue( analog_gain );

  myCamera->Camera.GevStreamChannelSelector.SetValue( GevStreamChannelSelector_StreamChannel0 );

  myCamera->Camera.BlackLevelSelector.SetValue (BlackLevelSelector_All );

  int64_t black_level_max = myCamera->Camera.BlackLevelRaw.GetMax();
  int64_t black_level_min = myCamera->Camera.BlackLevelRaw.GetMin();

  if ( black_level < black_level_min )
  {
    black_level = black_level_min;
    cerr << "Black Level too small.  Set to minimum of: " << black_level_min <<endl;
    flush(cerr);
  }

  if ( black_level > black_level_max )
  {
    black_level = black_level_max;
    cerr << "Black Level too large.  Set to maximum of: " << black_level_max <<endl;
    flush(cerr);
  }

  myCamera->Camera.BlackLevelRaw.SetValue ( black_level );

  // PacketSize
  myCamera->Camera.GevSCPSPacketSize.SetValue( 8192 );


  return true;
}
//*********************************

//*********************************
//Starts the camera producing streaming data, used after "setupCamera", before "snapCamera" or "stopCamera"
bool startCamera(CameraStruct* myCamera,int max_frame_rate)
{

  if (!myCamera->StreamGrabber.IsAttached())
  {
    myCamera->StreamGrabber.Attach(myCamera->Camera.GetStreamGrabber(0));
  }
  myCamera->StreamGrabber.Open();

 //Disable acquisition start trigger if available
  {
    GenApi::IEnumEntry* acquisitionStart = myCamera->Camera.TriggerSelector.GetEntry( TriggerSelector_AcquisitionStart);
    if ( acquisitionStart && GenApi::IsAvailable( acquisitionStart))
    {
      myCamera->Camera.TriggerSelector.SetValue( TriggerSelector_AcquisitionStart);
      myCamera->Camera.TriggerMode.SetValue( TriggerMode_Off);
    // Sets the internal camera frame rate 
    //  Camera.AcquisitionFrameRateEnable.SetValue(true);
    //  Camera.AcquisitionFrameRateAbs.SetValue(1.0);
    }
  }

  //Disable frame start trigger if available
  {
    GenApi::IEnumEntry* frameStart = myCamera->Camera.TriggerSelector.GetEntry( TriggerSelector_FrameStart);
   
    if ( frameStart && GenApi::IsAvailable( frameStart))
    {
      myCamera->Camera.TriggerSelector.SetValue( TriggerSelector_FrameStart);
      myCamera->Camera.TriggerMode.SetValue( TriggerMode_Off);
    }
  }
  //Set acquisition mode
  myCamera->Camera.AcquisitionMode.SetValue(AcquisitionMode_Continuous);

  myCamera->Camera.AcquisitionFrameRateEnable.SetValue( true );

  if ( max_frame_rate > 25 )
  {
    max_frame_rate = 25;
  } else if ( max_frame_rate < 1 )
  {
    max_frame_rate = 1;
  }

  myCamera->Camera.AcquisitionFrameRateAbs.SetValue( max_frame_rate );

  // Get the image buffer size
  const size_t ImageSize = (size_t)(myCamera->Camera.PayloadSize.GetValue());

  // We won't use image buffers greater than ImageSize
  myCamera->StreamGrabber.MaxBufferSize.SetValue(ImageSize);
 
  // We won't queue more than c_nBuffers image buffers at a time
  myCamera->StreamGrabber.MaxNumBuffer.SetValue(c_nBuffers);

  // Allocate all resources for grabbing. Critical parameters like image
  // size now must not be changed until FinishGrab() is called.
  myCamera->StreamGrabber.PrepareGrab();

  // Buffers used for grabbing must be registered at the stream grabber.
  // The registration returns a handle to be used for queuing the buffer.
  for (uint32_t i = 0; i < c_nBuffers; ++i)
  {
    CGrabBuffer *pGrabBuffer = new CGrabBuffer(ImageSize);
    pGrabBuffer->SetBufferHandle(myCamera->StreamGrabber.RegisterBuffer(
    pGrabBuffer->GetBufferPointer(), ImageSize));

    // Put the grab buffer object into the buffer list
    myCamera->BufferList.push_back(pGrabBuffer);
  }

  for (std::vector<CGrabBuffer*>::const_iterator x = myCamera->BufferList.begin(); x != myCamera->BufferList.end(); ++x)
  {
    // Put buffer into the grab queue for grabbing
    myCamera->StreamGrabber.QueueBuffer((*x)->GetBufferHandle(), NULL);
  }
 
  // Let the camera acquire images continuously ( Acquisiton mode equals
  // Continuous! )
  myCamera->Camera.AcquisitionStart.Execute();

  return true;
}
//*************************************

//*************************************
//Stops the cameras streaming data, used after "startCamera" or "snapCamera"
bool stopCamera(CameraStruct* myCamera)
{

    try
    {
        // Stop acquisition
        myCamera->Camera.AcquisitionStop.Execute();
    }
    catch (GenICam::LogicalErrorException e) 
    {
        cerr << "Caught exception from Acquisition Stop: LogicalErrorException " << endl;
        flush(cerr);
    }
    // Get the pending buffer back (You are not allowed to deregister
    // buffers when they are still queued)
    myCamera->StreamGrabber.CancelGrab();
    // Get all buffers back
    for (GrabResult r; myCamera->StreamGrabber.RetrieveResult(r););

    try
    {
        // Stop acquisition
        myCamera->Camera.AcquisitionStop.Execute();
    }
    catch (GenICam::LogicalErrorException e) 
    {
        cerr << "Caught exception from Acquisition Stop: LogicalErrorException " << endl;
        flush(cerr);
    }
    // Clean up
    // You must deregister the buffers before freeing the memory
    for (std::vector<CGrabBuffer*>::iterator it = myCamera->BufferList.begin(); it != myCamera->BufferList.end(); it++)
    {
        myCamera->StreamGrabber.DeregisterBuffer((*it)->GetBufferHandle());
        delete *it;
        *it = NULL;
    }

    // Free all resources used for grabbing
    myCamera->StreamGrabber.FinishGrab();

    // Close stream grabber
    myCamera->StreamGrabber.Close();

    //Clear the buffer list
    myCamera->BufferList.clear();

    return true;
}
//**************************************


//**************************************
//Gets the data into a frame, used after "startCamera"
int snapCamera(CameraStruct* myCamera)
{
  int status = 0;
  if (myCamera->StreamGrabber.GetWaitObject().Wait(3000))
  { 
    // Get the grab result from the grabber's result queue
    myCamera->StreamGrabber.RetrieveResult(myCamera->Frame);
    if (Failed == myCamera->Frame.Status())
    {
      // Error handling
      cerr << "No image acquired!" << endl;
      cerr << "Error code : 0x" << hex
      << myCamera->Frame.GetErrorCode() << endl;
      cerr << "Error description : "
      << myCamera->Frame.GetErrorDescription() << endl;
      flush(cerr);

      status = 1;
    }
  }
  else
  {
    //Timeout
    cerr << "Timeout occurred!" << endl;
    status = 2;
  }

  return status;
}

void getBPMRefImage(CameraStruct* myCamera, char* refImgAddr)
{
    std::string str(refImgAddr);
    //cout << "psnap: saving img to " << refImgAddr << endl;
    
    double* img = getFramePtr(myCamera);
    myCamera->monitor.saveImageToTxt(img, refImgAddr);
    return;
}
void findBPMForeground(CameraStruct* myCamera, char* imgAdrForForeground)
{
    std::string str(imgAdrForForeground);
    myCamera->monitor.clusterData(imgAdrForForeground);
    myCamera->monitor.findForeground();
    return;
}
void findBPMBeamParameter(CameraStruct* myCamera, char* imgAdrForBeamParam)
{

    myCamera->monitor.findRefBeamParam(imgAdrForBeamParam);
    return;
}
void findBPMScatterMap(CameraStruct* myCamera, char* imgAdrForScatMap)
{

    myCamera->monitor.find1stScatterMap(imgAdrForScatMap);
    return;
}
//*************************************

bool releaseCameraBuffer(CameraStruct* myCamera)
{
  myCamera->StreamGrabber.QueueBuffer(myCamera->Frame.Handle(),NULL);
  return true;
}
//***************************************

PyObject * getFrame(CameraStruct* myCamera)
{
  int  size = 0;

  if  (myCamera->Camera.PixelFormat.GetValue() == PixelFormat_Mono8)
  {
    size = myCamera->Frame.GetSizeX() * myCamera->Frame.GetSizeY();
    PyObject * newFrame;
    newFrame = PyBuffer_FromMemory(myCamera->Frame.Buffer(),size); 
    return newFrame;
  }
  else if (myCamera->Camera.PixelFormat.GetValue() == PixelFormat_Mono12)
  {
    size = myCamera->Frame.GetSizeX() * myCamera->Frame.GetSizeY() * 2;
    PyObject * newFrame;
    newFrame = PyBuffer_FromMemory(myCamera->Frame.Buffer(),size); 
    return newFrame;
  }
  else if (myCamera->Camera.PixelFormat.GetValue() == PixelFormat_BayerBG16)
  {
    size = myCamera->Frame.GetSizeX() * myCamera->Frame.GetSizeY() * 2;
    PyObject * newFrame;
    newFrame = PyBuffer_FromMemory(myCamera->Frame.Buffer(),size);
    return newFrame;
  }
  else if (myCamera->Camera.PixelFormat.GetValue() == PixelFormat_RGB8Packed)
  {
    size = myCamera->Frame.GetSizeX() * myCamera->Frame.GetSizeY() * 3;
    PyObject * newFrame;
    newFrame = PyBuffer_FromMemory(myCamera->Frame.Buffer(),size);
    return newFrame;
  }
  else
  {
    fprintf(stderr,"Unexpected Frame Format - neither Mono8 or Mono12 \n");
    return NULL;
  }
}

double* getFramePtr(CameraStruct* myCamera)
{
    void* buffer = myCamera->Frame.Buffer();
    unsigned char* pointer;
    int value;

    int pixNum = myCamera->monitor.imgr * myCamera->monitor.imgc;
    double* img = new double[pixNum];

    for (int i = 0; i < pixNum; i++) {

        pointer = (unsigned char*)buffer + i;
        value = *pointer;
        img[i] = (double)value;
        //cout << "img[307200-i] = " << value << endl;
    }
    return img;
}

double getBeamPosX(CameraStruct* myCamera)
{
    
    double* img = getFramePtr(myCamera);
    
    myCamera->monitor.zeroSatPixel(img);
    double result = myCamera->monitor.getPosX_deconvolveFirstOrder(img, myCamera->monitor.refw / 2);
    //cout << "psnap.cpp x = " << result << endl;
    delete[] img;
    return result;
}

double getBeamPosY(CameraStruct* myCamera)
{
    double* img = getFramePtr(myCamera);
    
    myCamera->monitor.zeroSatPixel(img);
    double result = myCamera->monitor.getPosY_deconvolveFirstOrder(img, myCamera->monitor.refw / 2);
    //cout << "psnap.cpp y = " << result << endl;
    delete[] img;
    return result;
}

