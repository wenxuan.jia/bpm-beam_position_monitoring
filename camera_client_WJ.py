#!/usr/bin/env python

import sys
import os
import getopt
import ConfigParser
import shlex, subprocess
from datetime import datetime
import time


def usage():
    print "-h, --help          Displays this message"
    print "-c, --config        Uses the settings in the config file listed"


#Default
host = "127.0.0.1"
port = "5000"


#Parse command line options
try:
   opts,args = getopt.getopt(sys.argv[1:],"hc:",["help","config="])
except getopt.GetoptError:
   usage()
   sys.exit(2)

if len(opts) == 0:
    usage()
    sys.exit()

for o, a in opts:

    if o in ("-h", "--help"):
        usage()
        sys.exit()
    elif o in ("-c", "--config"):
        configFile = str(a)
    else:
        usage()
        sys.exit()

#Create config parser and grab necessary configuration information
config = ConfigParser.ConfigParser()
config.read(configFile)
port = config.get('No Reload Camera Settings','Multicast Port')
multicastGroup = config.get('No Reload Camera Settings','Multicast Group')
cameraName = config.get('Camera Settings', 'Camera Name')

Done = False
path = ""
file_name = ""
user_file = ""
while not Done:
    now = datetime.utcnow()
    utctime = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + "-" + str(now.hour) + "-" + str(now.minute) + "-" + str(now.second)
    somename = raw_input("\nEnter some name:\n").strip()
    user_file = "/data/wenxuan.jia/BPM/Useful_images/testing/" + utctime + "_" + cameraName + "_" + somename + ".avi"
    
    path, file_name = os.path.split(user_file)
    if os.path.exists(path):
        if os.path.exists(user_file):
            already_exists_continue = raw_input("The file " + user_file + " already exists. Overwrite? y/N\n").lower()
            if 'y' == already_exists_continue or 'yes' == already_exists_continue:
                Done = True
        else:
            Done = True
    else:
        print "Path does not exist.\n"

dur = raw_input("\nEnter duration in minutes (0 for infinity):\n")
dur = int(dur)




commandString = ''.join(["gst-launch udpsrc multicast-group=",str(multicastGroup)," port=",str(port), " ! application/x-rtp, payload=127 ! rtph264depay ! ffdec_h264 ! ffmpegcolorspace ! ffenc_mpeg4 ! avimux ! filesink location=",str(user_file)])


if dur == 0:
    os.system(commandString)

else:
    

    rep = raw_input("\nEnter number of times to run (0 is no repeat): \n")
    rep = int(rep)
    if rep != 0:
	interval = raw_input("\nEnter how long it stops before next run in minutes: \n")
	interval = int(interval)

    else:
	interval = 0

    
    for i in range(rep+1):
	now = datetime.utcnow()
	utctime = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + "-" + str(now.hour) + "-" + str(now.minute) + "-" + str(now.second)
 	user_file = "/data/wenxuan.jia/BPM/Useful_images/testing/" + utctime + "_" + cameraName + "_" + somename + ".avi"
	commandString = ''.join(["gst-launch udpsrc multicast-group=",str(multicastGroup)," port=",str(port), " ! application/x-rtp, payload=127 ! rtph264depay ! ffdec_h264 ! ffmpegcolorspace ! ffenc_mpeg4 ! avimux ! filesink location=",str(user_file)])
	args = shlex.split(commandString)
	
        proc = subprocess.Popen(args)
	time.sleep(dur*60)
	proc.kill()
	time.sleep(interval*60)
	print "Captures done: " + str(i+1) + "\n"    




