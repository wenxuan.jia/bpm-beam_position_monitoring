#!/usr/bin/env python
import sys
import os
import shutil

NO_RELOAD_LINES = ['Height ','Width ','X ','Y ','Base Channel Name ','Camera IP ','Multicast Group ','Multicast Port ']


def usage():
    print "Pass this script a list of all old style camera ini files to update them to the new Config Parser compatible versions"
    print "Example:"
    print "convert_old_ini.py L1-CAM-PSL.ini L1-CAM-PRM.ini"


for ini_file in sys.argv[1:]:
    #Create a backup, but don't overwrite existing backups
    backup_file_name = ini_file + '.bak'
    x = 0
    while os.path.isfile(backup_file_name):
        x = x + 1
        backup_file_name = ini_file + str(x) + '.bak'

    try:
        print "Backing up " + ini_file + " to " + backup_file_name
        shutil.copyfile(ini_file,ini_file + '.bak')
    except:
        print "Unable to copy " + ini_file + " to " + ini_file + ".bak"
        print "Exiting"
        sys.exit(2)

    try:
        ini_file_handle = open(ini_file,'r')
    except:
        print "Unable to open or find file: " + str(ini_file)

    lines = ini_file_handle.readlines()
    ini_file_handle.close()

    camera_settings = ['[Camera Settings]\n']
    no_reload_camera_settings = ['\n','[No Reload Camera Settings]\n']

    for line in lines:
        if line.strip() in ['[Camera Settings]','[No Reload Camera Settings]','']:
            pass
        elif line.split('=')[0] in NO_RELOAD_LINES:
            no_reload_camera_settings.append(line)
        else:
            if 'Default ' in line:
                line = line[8:]
            elif 'Do ' in line:
                line = line[3:]
            camera_settings.append(line)

    ini_file_handle = open(ini_file,'w')
    for text in camera_settings:
        ini_file_handle.write(text)
    for text in no_reload_camera_settings:
        ini_file_handle.write(text)
