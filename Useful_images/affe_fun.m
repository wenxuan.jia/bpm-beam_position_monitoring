function err = affe_fun(c1, c2, c3, c4, c5, c6, posx, posy, img, ref)
    % RMS error for linearized affine transformation
    
    [xlen, ylen] = size(img);
    [yg, xg] = meshgrid(1:ylen, 1:xlen);
    
    pmatrix = [posx'; posy'; ones(1, length(posx))];
    
    %        [c1 c2 c3]
    % affc = [c4 c5 c6]
    %        [ 0  0  1]
    affc = [c1 c2 c3; c4 c5 c6; 0 0 0];
    affc = affc + eye(3,3);
    
    transformed = affc*pmatrix;
    tfposx = transformed(1,:)';
    tfposy = transformed(2,:)';
    
    tfdat = interp2(yg, xg, img, tfposy, tfposx);
    % flipped indices
    
    err = (tfdat - double(ref)).^2;
    err(isnan(err)) = 0; % remove those outside img after interp2
    
    err = sqrt( sum(err) );
    
    
end