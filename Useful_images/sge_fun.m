function err = sge_fun(affc, prevsg, currsg)
    % RMS error for scatter map
    
    [xlen, ylen] = size(prevsg);
    [yg, xg] = meshgrid(1:ylen, 1:xlen);
    
    id = prevsg == 0 | isnan(prevsg);
    id = ~id;
    posx = xg(id); posy = yg(id);
    
    pmatrix = [posx'; posy'; ones(1, length(posx))];
    
    %        [c1 c2 c3]
    % affc = [c4 c5 c6]
    %        [ 0  0  0]
%     affc = [0 0 dx; 0 0 dy; 0 0 0];
    affc = [affc; 0 0 0];
    affc = affc + eye(3,3);
    
    transformed = affc*pmatrix;
    tfposx = transformed(1,:)';
    tfposy = transformed(2,:)';
    
    tfdat = interp2(yg, xg, prevsg, tfposy, tfposx);
    % flipped indices
        
    err = sqrt( nansum((tfdat - currsg(id)).^2) );
    
    
end