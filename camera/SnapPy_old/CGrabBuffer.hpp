#ifndef PYLONINCLUDES
#include <pylon/PylonIncludes.h>
#include <pylon/gige/BaslerGigECamera.h>
#define PYLONINCLUDES
#endif

#ifndef CGRABBUFFER
#define CGRABBUFFER

typedef Pylon::CBaslerGigECamera Camera_t;
using namespace Basler_GigECameraParams;
using namespace Basler_GigEStreamParams;


using namespace Pylon;
using namespace std;



// Constants
// Buffers for grabbing
const uint32_t c_nBuffers = 10;

//*******************************
//CGrabBuffer Class
class CGrabBuffer
{
    public:
        CGrabBuffer(const size_t ImageSize);
        ~CGrabBuffer();
        uint8_t* GetBufferPointer(void) { return m_pBuffer; }
        StreamBufferHandle GetBufferHandle(void) { return m_hBuffer; }
        void SetBufferHandle(StreamBufferHandle hBuffer) { m_hBuffer = hBuffer; };

    protected:
        uint8_t *m_pBuffer;
        StreamBufferHandle m_hBuffer;
};

#endif
