function err = gausse_fun(x, y, img, I0, w) % img has NaN
    % RMS error for Gaussian
    
    [xlen, ylen] = size(img);
    [yg, xg] = meshgrid(1:ylen, 1:xlen); % image indexing is flipped
    
    rg = sqrt((xg-x).^2 + (yg-y).^2);
    ig = I0.*exp(-2*rg.^2./w.^2);
    
    err = sqrt(nansum(nansum( (img-ig).^2 )));
    
end