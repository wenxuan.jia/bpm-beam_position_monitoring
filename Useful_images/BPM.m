set(0, 'DefaultAxesFontName', 'Times');
% or any of the following...
set(0, 'defaultUicontrolFontName', 'Times');
set(0, 'defaultUitableFontName', 'Times');
set(0, 'defaultAxesFontName', 'Times');
set(0, 'defaultTextFontName', 'Times');
set(0, 'defaultUipanelFontName', 'Times');

set(0, 'DefaultFigureWindowStyle', 'docked')

set(0, 'DefaultLineLineWidth', 1);
set(0,'defaultAxesFontSize', 12);




if 0
    
%     figure; 
%     for i = 1:length(channel)
%         freq = length(q(i).data)/q(i).duration;
%         t = 1/freq:1/freq:q(i).duration;
%         subplot(2,3,i); plot(t, q(i).data);
%         xlabel(['Time (' num2str(freq) ' Hz)'])
%         title(channel{i}, 'interpreter', 'none')
%         grid on;
%     end
%     return
    
    
    [yg, xg] = meshgrid(1:640, 1:480); % image indexing is flipped 
    
    x0 = 270; y0 = 270;
    
    I0 = 550; w = 60;
    
    ig0 = I0.*exp(-2*((xg-x0).^2 + (yg-y0).^2)./w.^2);
    
    x1 = 275; y1 = 270;

    ig1 = I0.*exp(-2*((xg-x1).^2 + (yg-y1).^2)./w.^2);
    
    sum(sum(ig0-ig1))
    
    delta = 20;
    ig01 = I0.*exp(-2*((xg-x0).^2 + (yg-y0+delta).^2)./w.^2) - I0.*exp(-2*((xg-x0).^2 + (yg-y0).^2)./w.^2);
    
    sum(sum(ig01.*ig1))
    
    
    
    exp(-((x0-x1)^2 + (y0-y1)^2)/w^2)*I0^2*2*pi*w^2/8*(exp((2*(y0-y1)*delta-delta^2)/w^2)-1)
    
    
    
    return
    
    
end


%% main
if 1
    
    
    [refimg, filename] = loadVideo('name', ['*2022-05-13-18*ITMY*'], 'frame', [1 25], 'avg', 25);
    
    
    refimg_s = refimg{1}; % reference image, _s means saturated
    noiseThreshold = 50; % from 1 to 255. Only used for clustering.
    clusterDist = 10; % pixel
    clusterMin = 20; % minimal pixels to form a background cluster
    
    [fgft, bgft] = ftDetector(refimg_s, noiseThreshold, clusterDist, clusterMin);
    
    
    fg1 = invalidate(double(refimg{1}).*fgft.polyid);
    
    [posx1, posy1, sg1, I0, w] = find1stScat(fg1, 220, 350); % priori values can be entered
    
%     
%     
% %     [rawimg, filename] = loadVideo('name', ['*2022-05-13-18*ITMY*'], 'frame', [600 800], 'avg', 25);
    [rawimg, filename] = loadVideo('name', ['*2022-05-13-18*ITMY*'], 'frame', [800 1350], 'avg', 2);
    
    
    mirx = []; miry = [];
    posx_gd = []; posy_gd = [];
    posx_fo = []; posy_fo = [];
    posx_fo2 = []; posy_fo2 = [];
    
    prevx = posx1; prevy = posy1;
    prevsg = sg1; prevfg = fg1;
    
    for j = 1:length(rawimg)
        
        if ~any(any(rawimg{j}))
            posx_gd = [posx_gd; NaN]; posy_gd = [posy_gd; NaN];
            posx_fo = [posx_fo; NaN]; posy_fo = [posy_fo; NaN];
            posx_fo2 = [posx_fo2; NaN]; posy_fo2 = [posy_fo2; NaN];
            
            mirx = [mirx; NaN]; miry = [miry; NaN];
            continue
            
        end
    
    
    
    currimg = invalidate(rawimg{j});
    
%     currimg = subtractCamMotion(currimg);
    
    currfg = double(currimg).*fgft.polyid;
    currfg = currfg/nansum(nansum(currfg))*nansum(nansum(prevfg));
    % normalize saturation
    
    
    
%     [roughx, roughy] = pos_fo2(currfg./prevsg, prevx, prevy, I0, w);
%     posx_fo2 = [posx_fo2; roughx]; posy_fo2 = [posy_fo2; roughy];
    
    [roughx, roughy] = pos_fo(currfg, prevx, prevy, I0, w, prevsg);
    posx_fo = [posx_fo; roughx]; posy_fo = [posy_fo; roughy];
    
%     currsg = getScatterMap(currfg, roughx, roughy, I0, w);
%     
%     
%     [sx, sy, currsg] = getScatterMotion(prevsg, currsg);
%     % sx, sy are coordinate transformation, so the thing on image would
%     % move -sx, -sy, relative to previous image
%     mirx = [mirx; -sx]; miry = [miry; -sy];
%     
%     
%     [finerx, finery] = pos_gd(currfg./currsg, prevx, prevy, I0, w);
%     posx_gd = [posx_gd; finerx]; posy_gd = [posy_gd; finery];
%     
%     prevx = finerx; prevy = finery;
%     prevsg = currsg; prevfg = currfg; 
    
    fprintf('Beam center: (%.2f, %.2f)\n', posx_fo(end), posy_fo(end))
    
    end
    
        
    return
end


%% Subtract camera motion
if 0
    
    % optical flow
    
    
    
    % linearized affine
    % The error isn't as smooth as gaussian beam. Hard to find the ideal
    % slope where error is zero
    % need some smoothing, low-passing before descent
    % need smarter descent algorithm
    % http://www.codinglabs.net/article_world_view_projection_matrix.aspx
    
    %%%%%%%%%%%%%%%%%%%
    img = rawimg{1}; % image to detect
    
    %%%%%%%%%%%%%%%%%%%
    
    tmp = img;
    for x = 5:475
        tmp(x,:) = img(x-2, :);
    end
    img = tmp;
    
    
    
%     [xlen, ylen] = size(img);
%     [yg, xg] = meshgrid(1:ylen, 1:xlen);
    
    
    
%     affc = @(c1, c2, c3, c4, c5, c6) [c1 c2 c3; c4 c5 c6; 0 0 1];
    %        [c1 c2 c3]
    % affc = [c4 c5 c6]
    %        [ 0  0  1]
    
    
%     for j = 1:length(bgft)
        
    j = 1;
    
        ref = bgft(j).ref;
        polyid = bgft(j).polyid; % big polygon enclosing the feature
        
        imgft = double(img).*double(polyid); % image with only feature j
        imgft = imgft/sum(sum(imgft))*sum(sum(double(ref)));
        
        posx1 = bgft(j).posx;
        posy1 = bgft(j).posy;
        
        
        
        
        
        
        cstart = 0; cstep = 0.01; % roughly pixel unit
        c1 = []; c2 = []; c3 = []; c4 = []; c5 = []; c6 = [];
        c1(1) = cstart/bgft(j).cx; % same relative change as c3, c6.
        c2(1) = cstart/bgft(j).cy;
        c3(1) = cstart; % x displacement
        c4(1) = cstart/bgft(j).cx;
        c5(1) = cstart/bgft(j).cy;
        c6(1) = cstart; % y displacement
        
        
        stepsize = 0.01;
        it = 0;
        stepmoved = Inf;
        
        
        while stepmoved(end) > 0.01
            it = it+1;
            
            dedc(1) = affe_fun(c1(end)+cstep/bgft(j).cx, c2(end), c3(end), c4(end), c5(end), c6(end), posx1, posy1, imgft, ref) ...
                - affe_fun(c1(end), c2(end), c3(end), c4(end), c5(end), c6(end), posx1, posy1, imgft, ref);
            dedc(2) = affe_fun(c1(end), c2(end)+cstep/bgft(j).cy, c3(end), c4(end), c5(end), c6(end), posx1, posy1, imgft, ref) ...
                - affe_fun(c1(end), c2(end), c3(end), c4(end), c5(end), c6(end), posx1, posy1, imgft, ref);
            dedc(3) = affe_fun(c1(end), c2(end), c3(end)+cstep, c4(end), c5(end), c6(end), posx1, posy1, imgft, ref) ...
                - affe_fun(c1(end), c2(end), c3(end), c4(end), c5(end), c6(end), posx1, posy1, imgft, ref);
            dedc(4) = affe_fun(c1(end), c2(end), c3(end), c4(end)+cstep/bgft(j).cx, c5(end), c6(end), posx1, posy1, imgft, ref) ...
                - affe_fun(c1(end), c2(end), c3(end), c4(end), c5(end), c6(end), posx1, posy1, imgft, ref);
            dedc(5) = affe_fun(c1(end), c2(end), c3(end), c4(end), c5(end)+cstep/bgft(j).cy, c6(end), posx1, posy1, imgft, ref) ...
                - affe_fun(c1(end), c2(end), c3(end), c4(end), c5(end), c6(end), posx1, posy1, imgft, ref);
            dedc(6) = affe_fun(c1(end), c2(end), c3(end), c4(end), c5(end), c6(end)+cstep, posx1, posy1, imgft, ref) ...
                - affe_fun(c1(end), c2(end), c3(end), c4(end), c5(end), c6(end), posx1, posy1, imgft, ref);
            
            dedc = dedc/cstep;
            
%             c1(it+1) = c1(it) - stepsize*dedc(1);
%             c2(it+1) = c2(it) - stepsize*dedc(2);
            c3(it+1) = c3(it) - stepsize*dedc(3);
%             c4(it+1) = c4(it) - stepsize*dedc(4);
%             c5(it+1) = c5(it) - stepsize*dedc(5);
            c6(it+1) = c6(it) - stepsize*dedc(6);
            
            stepmoved = [stepmoved; stepsize*sqrt(sum(dedc.^2))];
            
            
        end
        
        fprintf('Unweighted affine matrix - I = \n');
        fprintf('[%.3f %.3f %.3f]\n', c1(end), c2(end), c3(end));
        fprintf('[%.3f %.3f %.3f]\n', c4(end), c5(end), c6(end));
        fprintf('[   0      0     0   ] for feature %g \n', j);
        
        bgft(j).c1 = c1(end)/bgft(j).cx;
        bgft(j).c2 = c2(end)/bgft(j).cy;
        bgft(j).c3 = c3(end);
        bgft(j).c4 = c4(end)/bgft(j).cx;
        bgft(j).c5 = c5(end)/bgft(j).cy;
        bgft(j).c6 = c6(end);
        
        bgft(j).err = affe_fun(c1(it), c2(it), c3(it), c4(it), c5(it), c6(it), posx1, posy1, imgft, ref);
        

        
        
        
%     end % feature
    
    
    
    
    
end


%% Test different algorithm
if 1
    
    
    if 1
        figure;
        subplot(2,3,1);
        histogram(x_gd-x_real)
        hold on; histogram(x_fo-x_real)
        xlabel('X error [pixel]')
        ylabel('Counts')
        legend('Gradient descent', 'Deconvolving first-order')
        legend boxoff
        
        subplot(2,3,2);
        histogram(y_gd-y_real)
        hold on; histogram(y_fo-y_real)
        xlabel('Y error [pixel]')
        ylabel('Counts')
%         legend('Gradient descent', 'Deconvolving first-order')
%         legend boxoff
        
        subplot(2,3,3)
        histogram(t_gd); hold on; histogram(t_fo)
        xlabel('Time used [sec]')
        ylabel('Counts')
%         legend('Gradient descent', 'Deconvolving first-order')
%         legend boxoff
        
        subplot(2,3,4)
        plot(x_real-x0, x_gd-x_real, '.')
        hold on; plot(x_real-x0, x_fo-x_real, '.')
        axis tight
        xlabel('Real X motion [pixel]')
        ylabel('Error')
        
        subplot(2,3,5)
        plot(y_real-y0, y_gd-y_real, '.')
        hold on; plot(y_real-y0, y_fo-y_real, '.')
        axis tight
        xlabel('Real Y motion [pixel]')
        ylabel('Error')
        
        subplot(2,3,6)
        plot(sqrt((x_real-x0).^2 + (y_real-y0).^2), t_gd, '.'); hold on;
        plot(sqrt((x_real-x0).^2 + (y_real-y0).^2), t_fo, '.');
        xlabel('Real total motion [pixel]')
        ylabel('Time used [sec]')
        
        return
    end
    
    
    
    [yg, xg] = meshgrid(1:640, 1:480);
    x_gd = []; y_gd = []; t_gd = [];
    x_fo = []; y_fo = []; t_fo = [];
    x_real = []; y_real = [];
    
    
    di = 0;
    
    
    
    while(true)
%     for fasd = 1:100
        di = di+1;
        
        
    % sample images
    
    
%     I0 = 550; w = 60; 
    x0 = 212; y0 = 300;
%     ig0 = I0.*exp(-2*((xg-x0).^2 + (yg-y0).^2)./w.^2);
%     sg = rand(480, 640)/2+1;
    sg1 = currsg;
%     img = uint8(ig0.*sg);
    
    
    x1 = x0 + 6*(rand-0.5); y1 = y0 + 6*(rand-0.5);
    x_real = [x_real; x1]; y_real = [y_real; y1];
%     x1 = x0; y1 = y0;
    
    ig1 = I0.*exp(-2*((xg-x1).^2 + (yg-y1).^2)./w.^2);
    ig1 = ig1.*sg1;
    ig1 = uint8(ig1);
    ig1 = double(ig1);
    ig1(ig1 == 255) = NaN;
    
    
    % Deconvolve scatter map
    ig1 = ig1./sg1;
    ig1(isinf(ig1)) = NaN;
    
    
    imgs = ig1;
    
        
    tic;
    [roughx, roughy] = pos_fo(ig1, x0, y0, I0, w);
    tmp = toc;
    
    t_fo = [t_fo; tmp];
    x_fo = [x_fo; roughx];
    y_fo = [y_fo; roughy];
    


    
    tic;
    [finerx, finery] = pos_gd(ig1, x0, y0, I0, w);
    % imgs has only foreground with NaN in it.
    tmp = toc;
    
    t_gd = [t_gd; tmp];
    x_gd = [x_gd; finerx];
    y_gd = [y_gd; finery];
    
    
    
    
    
    % find center by convoluting (0,0) mode
    
    % test image
%     [xlen, ylen] = size(img);
%     [yg, xg] = meshgrid(1:ylen, 1:xlen); % image indexing is flipped
%     img = uint8(I0.*exp(-2*((xg-278).^2 + (yg-355).^2).^2./w.^2));
    
%     rlargerthan1 = ceil(sqrt(log(I0)*w^2/2));
%     
%     [yg, xg] = meshgrid(1:2*rlargerthan1, 1:2*rlargerthan1); % image indexing is flipped 
%     
%     ig = I0.*exp(-2*((xg-rlargerthan1).^2 + (yg-rlargerthan1).^2)./w.^2);
%     
%     % regulate two images
%     ig(ig > 255) = 0; 
%     img(isnan(img)) = 0;
%     
%     % convolution
%     res = conv2(img, ig, 'valid');
%     % conv2(A,B,'valid') puts B around A, and inside A
%     % assuming the beam is not near border
%     % result has size (1+i_A-i_B, 1+j_A-j_B)
%     
%     [xlen, ylen] = size(img);
%     [yg, xg] = meshgrid(1:1+ylen-2*rlargerthan1, 1:1+xlen-2*rlargerthan1);
%     convx = xg(res == max(max(res)));
%     convy = yg(res == max(max(res)));
%     
%     posx_conv00(imgnum) = convx + rlargerthan1;
%     posy_conv00(imgnum) = convy + rlargerthan1;
%     
%     
%     
% %     figure; imshow(uint8(ig0));
%     figure; surf(res); shading interp; colormap('jet'); view([0 90])
    
    
%     end %imgnum loop



    fprintf('%d loop done\n', di);
%     fprintf('real answer:      (%g, %g)\n', x1(end), y1(end))
%     fprintf('centroid   :      (%g, %g)\n', posx_ct(end), posy_ct(end))
%     
%     fprintf('brute force:      (%g, %g)\n', posx_bf(end), posy_bf(end));
%     fprintf('gradient descent: (%g, %g)\n', posx_gd(end), posy_gd(end));
%     fprintf('first order:      (%g, %g)\n', posx_fo(end), posy_fo(end));
    
    end
    
    
    
    
    
end


%% Read image and pre-processing
function [rawimg, filename] = loadImage(name)
%     name = ['*2018-10-31*'];

    %search file with the name specified
    files = dir(['./', name]);
    %search subfolders
    files = [files; dir(['**/', name])];
    
    
    i = 1;
    if(isempty(files))
        error('\n\n No data found with filename ''%s''!!!\n\n', name);

    elseif(length(files) > 1)
        fprintf('\n More than one data found with filename ''%s'':\n', name);
        fprintf('    0: Load all of the following:\n')
        for j = 1:length(files)
            fprintf('    %d: %s\n', j, files(j).name);
        end
        fprintf('\n');
        i = input(' Please choose the one by index: ');

    end
    
    rawimg = {};
    
    if i == 0
        filename = {};
        for j = 1:length(files)
            fileadr = [files(j).folder, '/', files(j).name];
            filename = [filename; files(j).name];
            rawimg = [rawimg; imread(fileadr)];
        end
        
    else
        
        fileadr = [files(i).folder, '/', files(i).name];
        filename = files(i).name;
        rawimg = [rawimg; imread(fileadr)];
        
    end
    
    
    
    % Calibrate exposure
    
    
    % Calibrate power
    
    % Subtract the reaction mass reflection behind test mass
    
    
    
    return
    
end


function [rawimg, filename] = loadVideo(varargin) % output is double instead of uint8
    for i = 1:2:length(varargin)
        temp = varargin{i+1};
        eval([varargin{i} '= temp;'])
    end
    
    if ~exist('avg', 'var')
        avg = 25; % default fps. Output frequency = fps/avg
    end
    
    if ~exist('frame', 'var')
        error('\n\n Please specify frames to input!!!\n\n');
    end
    
    
%     name = ['*2018-10-31*'];

    %search file with the name specified
    files = dir(['./', name]);
    %search subfolders
    files = [files; dir(['**/', name])];
    
    
    i = 1;
    if(isempty(files))
        error('\n\n No data found with filename ''%s''!!!\n\n', name);

    elseif(length(files) > 1)
        fprintf('\n More than one data found with filename ''%s'':\n', name);
        fprintf('    0: Load all of the following:\n')
        for j = 1:length(files)
            fprintf('    %d: %s\n', j, files(j).name);
        end
        fprintf('\n');
        i = input(' Please choose the one by index: ');

    end
    
    rawimg = {};
    
    if i == 0
%         filename = {};
%         for j = 1:length(files)
%             fileadr = [files(j).folder, '/', files(j).name];
%             filename = [filename; files(j).name];
%             rawimg = [rawimg; imread(fileadr)];
%         end
        
    else
        
        fileadr = [files(i).folder, '/', files(i).name];
        filename = files(i).name;
        videoReader = VideoReader(fileadr);
        frames = read(videoReader, frame);
        [~, ~, ~, framenum] = size(frames);
        
        RGB = 1; % the RGB layers are identical, so pick whatever one
        
        for j = 0:floor(framenum/avg)-1
            imgsum = zeros(480, 640);
            avgnum = 0;
            for k = 1:avg
                tmp = double(frames(:,:,RGB,j*avg+k));
                if mode(mode(tmp)) ~= 128 % bad image
                    imgsum = imgsum + tmp;
                    avgnum = avgnum + 1;
                end
            end
            
            rawimg = [rawimg; (imgsum/avgnum)];
        end
        
        
        
        
    end
    
    
    
    % Calibrate exposure
    
    
    % Calibrate power
    
    
    
    % Subtract the reaction mass reflection behind test mass
    
    
    
    return
    
end




