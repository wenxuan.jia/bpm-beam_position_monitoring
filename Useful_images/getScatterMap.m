function sg = getScatterMap(fg, x, y, I0, w)
    [xlen, ylen] = size(fg);
    [yg, xg] = meshgrid(1:ylen, 1:xlen); % image indexing is flipped
    
    rg = sqrt((xg-x).^2 + (yg-y).^2);
    sg = double(fg)./( I0.*exp(-2*rg.^2./w.^2) ); 


end