﻿// test.cpp : Defines the entry point for the application.
//

#include "test.h"



using namespace std;

int main()
{

	BeamPosMon m;
	double* img = new double[480 * 640];


	string opt_infile = "G:/My Drive/LIGO/BPM/Ccode/test/test/images/0513_20WBeamDrifting/refimg.txt";
	m.clusterData(opt_infile);
	m.findForeground();

	m.loadImagesFromTxt(img, opt_infile);

	m.findRefBeamParam(opt_infile);	
	cout << m.refX << endl;
	cout << m.refY << endl;
	cout << m.refI0 << endl;
	cout << m.refw << endl;

	/*m.refX = 221.734;
	m.refY = 312.669;
	m.refI0 = 163.657;
	m.refw = 68.7677;*/
	m.find1stScatterMap(opt_infile);

	

	//
	vector<double> xlist, ylist;
	double pwr = 0;
	double* result = new double[2];
	for (int i = 0; i < 100; i++) {
		opt_infile = "G:/My Drive/LIGO/BPM/Ccode/test/test/images/0513_20WBeamDrifting/img" + to_string(i) + ".txt";
		m.loadImagesFromTxt(img, opt_infile);

		m.zeroSatPixel(img);
		/*for (int i = 0; i < m.fgn; i++) {
			pwr += img[m.fgid[i]];
		}
		cout << to_string(pwr) << endl;*/

		m.getPos_deconvolveFirstOrder(result, img, m.refw / 2, m.refw / 2);
		xlist.push_back(result[0]);
		ylist.push_back(result[1]);
		cout << result[0] << " " << result[1] << endl;
		cout << m.getPosX_deconvolveFirstOrder(img, m.refw / 2) << " " << m.getPosY_deconvolveFirstOrder(img, m.refw / 2) << endl;
	}

	string opt_outfile = "G:/My Drive/LIGO/BPM/Ccode/test/test/images/0513_20WBeamDrifting/result.txt";
	FILE* f = fopen(opt_outfile.c_str(), "w");
	if (!f) {
		fprintf(stderr, "Cannot open '%s'\n", opt_outfile.c_str());
		return 0;
	}

	for (int i = 0; i < xlist.size(); i++) {

		fprintf(f, "%lf, %lf\n", xlist[i], ylist[i]);
	}
	fclose(f);







	//int** h = new int* [npar]; // each element is a pointer to an array.

	//for (int i = 0; i < npar; i++)
	//	h[i] = new int[npar]; // build rows

	//for (int i = 0; i < npar; i++)
	//{
	//	for (int j = 0; j < npar; ++j)
	//	{
	//		h[i][j] = i+j;
	//		cout << h[i][j] << endl;
	//	}
	//}

	//
	//for (int i = 0; i < npar; i++)
	//	delete[] h[i];

	//delete[] h;


	/*double x[8] = { 0,1,2,4,0,1,3,3 };
	double y[8] = { 3,1,2,4,0,2,1,3 };*/
	
	
	/*double* img = new double[480*640];
	int id = 0;
	for (int i = 0; i < 480 * 640; i++) {
		img[id] = 245.4;
		id++;
	}
	m.zeroSatPixel(img);*/

	




	//m.lmstat_verbose = 1;
	//m.findForeground();
	//m.findRefBeamParamKnownXY(img, 262.4223, 296.4610);
	//m.findRefBeamParam(img);
	


	/*double par[2] = { 440, 60 };
	double y[1] = { 4.000000 };
	double fdata[1] = {130.628089};

	cout << error_func(par, 1, y, nullptr, BeamPosMon::LMfunc_calcI, fdata) << endl;*/

	//m.clusterData();
	//m.findForeground();



	
	/*Matrix m1(2, 2);
	m1.set(0, 0, 1.2);
	m1.set(0, 1, 0.4);
	m1.set(1, 0, 0.6);
	m1.set(1, 1 ,1.9);
	
	Matrix m2(2, 2);
	m2.set(0, 0, 2);
	m2.set(0, 1, 0);
	m2.set(1, 0, 1);
	m2.set(1, 1, 2);

	
	Matrix m3 = m1 *= m2;

	m3.print();*/


	/*cout << sizeof(img[0]) / sizeof(img[0][0]) << endl;
	cout << img[1][0] << endl;*/

	/*cout << img[0][0] << endl;
	cout << img[0][1] << endl;

	cout << img[1][0] << endl;*/


	/*BeamPosMon m (3, 4);

	
	cout << m.area() << endl;*/
	return 0;
}
