#!/usr/bin/env python
import sys
import getopt

def usage():
    print "-h, --help       Displays this help message"
    print "-i, --ifo        Set the IFO (i.e. L1, H1)"

try:
    opts,args = getopt.getopt(sys.argv[1:],"hi:",["help","ifo="])
except getopt.GetoptError:
    usage()
    sys.exit(2)

if len(opts) == 0:
    usage()
    sys.exit()

for o, a in opts:
    if o in ("-h", "--help"):
        usage()
        sys.exit()
    elif o in ("-i", "--ifo"):
        ifo = str(a)
    else:
        usage()
        sys.exit()


camera_list = ['AS','BS','ETMX','ETMY','IM4_TRANS','ITMX','ITMY','MC_REFL','MC_TRANS','MC1','MC2','MC3','MC1_LAV','MC2_LAV','MC3_LAV','OMC_REFL','OMC_TRANS','PR2','PR3','PRM','PRM_REFL_180','PRM_REFL_90','PSL','SPARE1','TEMP1','TEMP2','HAM5_LAV','SR2','SR3','OMC_BENCH','AS_WFS','SQZ_SHG_TRANS','SQZ_OPO_TRANS_GREEN','SQZ_OPO_TRANS_IR','X_IR_TRANS','X_GRN_TRANS','Y_GRN_TRANS','PRM_LAV','PR2_LAV']
default_string = '''
grecord(ai,"YYYYY:CAM-XXXXX_X")
{
  field(DESC, "GigE Cam X centroid")
  field(PREC, "4")
  field(LINR, "NO CONVERSION")
}

grecord(ai,"YYYYY:CAM-XXXXX_Y")
{
  field(DESC,"GigE Cam Y centroid")
  field(PREC,"4")
  field(LINR,"NO CONVERSION")
}

grecord(ai,"YYYYY:CAM-XXXXX_WX")
{
  field(DESC, "GigE Gaussian W X")
  field(PREC, "4")
  field(LINR, "NO CONVERSION")
}

grecord(ai,"YYYYY:CAM-XXXXX_WY")
{
  field(DESC,"GigE Gaussian W Y")
  field(PREC,"4")
  field(LINR,"NO CONVERSION")
}

grecord(ai,"YYYYY:CAM-XXXXX_XY")
{
  field(DESC,"GigE Gaussian W XY")
  field(PREC,"4")
  field(LINR,"NO CONVERSION")
}

grecord(ai, "YYYYY:CAM-XXXXX_SUM")
{
  field(DESC,"GigE Pixel Sum")
  field(PREC,"4")
  field(LINR,"NO CONVERSION")
}

grecord(ai,"YYYYY:CAM-XXXXX_EXP")
{
  field(DESC,"GigE Cam Exposure")
  field(PREC, "0")
  field(LINR,"NO CONVERSION")
  field(HOPR,"100000")
  field(LOPR,"0")
}

grecord(bi,"YYYYY:CAM-XXXXX_SNAP")
{
  field(DESC,"GigE Cam Snapshot ctrl")
  field(ONAM,"SNAPSHOT")
  field(ZNAM,"OFF")
}

grecord(stringin,"YYYYY:CAM-XXXXX_FILE")
{
  field(DESC,"File name for snapshots")
}


grecord(bi,"YYYYY:CAM-XXXXX_RELOAD")
{
  field(DESC,"GigE Cam configuration reload")
  field(ONAM,"RELOAD")
  field(ZNAM,"OFF")
}

grecord(bi,"YYYYY:CAM-XXXXX_AUTO")
{
  field(DESC,"GigE Cam auto exposure")
  field(ONAM,"ON")
  field(ZNAM,"OFF")
}

grecord(ai,"YYYYY:CAM-XXXXX_ARCHIVE_INTERVAL")
{
  field(DESC,"GigE Cam Archive Minute Interval")
  field(PREC, "0")
  field(LINR,"NO CONVERSION")
  field(HOPR,"1440")
  field(LOPR,"1")
}

grecord(bi,"YYYYY:CAM-XXXXX_ARCHIVE_RESET")
{
  field(DESC,"GigE Cam Archive Timer Reset")
  field(ONAM,"RESET")
  field(ZNAM,"OFF")
}

grecord(stringin,"YYYYY:CAM-XXXXX_CONFIG_FILE")
{
  field(DESC,"File name for config file")
}

'''

default_string = default_string.replace('YYYYY',ifo)

filehandle1 = open('./camera.db','w')
for camera in camera_list:
    print_string = default_string.replace('XXXXX',camera)
    filehandle1.write(print_string)
filehandle1.close()

ini_header = '''[default]
gain=1.00
datatype=4
ifoid=0
slope=6.1028e-05
acquire=1
offset=0
units=V
dcuid=4
datarate=16

'''

ini_string = '''[YYYYY:CAM-XXXXX_X]
[YYYYY:CAM-XXXXX_Y]
[YYYYY:CAM-XXXXX_WX]
[YYYYY:CAM-XXXXX_WY]
[YYYYY:CAM-XXXXX_XY]
[YYYYY:CAM-XXXXX_SUM]
[YYYYY:CAM-XXXXX_EXP]
[YYYYY:CAM-XXXXX_SNAP]
[YYYYY:CAM-XXXXX_FILE]
[YYYYY:CAM-XXXXX_RELOAD]
[YYYYY:CAM-XXXXX_AUTO]
[YYYYY:CAM-XXXXX_ARCHIVE_RESET]
[YYYYY:CAM-XXXXX_ARCHIVE_INTERVAL]
'''

ini_string = ini_string.replace('YYYYY',ifo)

filehandle2 = open('./' + str(ifo) + 'EDCU_CAM.ini','w')
filehandle2.write(ini_header)
for camera in camera_list:
    print_string = ini_string.replace('XXXXX',camera)
    filehandle2.write(print_string)
filehandle2.close()

