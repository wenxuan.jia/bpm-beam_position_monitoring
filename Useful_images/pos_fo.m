function [posx_fo, posy_fo] = pos_fo(fg, x0, y0, I0, w, sg) % no scatter map divided
%     fg(isinf(fg)) = NaN;
    
    [xlen, ylen] = size(fg);
    [yg, xg] = meshgrid(1:ylen, 1:xlen); % image indexing is flipped
    
    
    deltax = w/2;
    deltay = w/2;
    
    ig10 = I0.*exp(-2*((xg-x0+deltax).^2 + (yg-y0).^2)./w.^2) - I0.*exp(-2*((xg-x0-deltax).^2 + (yg-y0).^2)./w.^2);
    ig01 = I0.*exp(-2*((xg-x0).^2 + (yg-y0+deltay).^2)./w.^2) - I0.*exp(-2*((xg-x0).^2 + (yg-y0-deltay).^2)./w.^2);
    
    coeff10 = [];
    coeff01 = [];
    
    
    delta = [-5 0 5];
    for d = delta
        
        coeff10 = [coeff10 nansum(nansum(ig10.*sg.*I0.*exp(-2*((xg-(x0+d)).^2 + (yg-y0).^2)./w.^2)))];
        coeff01 = [coeff01 nansum(nansum(ig01.*sg.*I0.*exp(-2*((xg-x0).^2 + (yg-(y0+d)).^2)./w.^2)))];
        
    end
    
    posx_fo = x0 + interp1(coeff10, delta, nansum(nansum(ig10.*fg)), 'linear', 'extrap');
    posy_fo = y0 + interp1(coeff01, delta, nansum(nansum(ig01.*fg)), 'linear', 'extrap');
end
