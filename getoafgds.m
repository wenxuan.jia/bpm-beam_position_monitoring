clear oot
date=['jul 21 2022 02:30:00 UTC']; 

% t0  = gps(date);
% t0  = 1342331297; % 4.4 dB ref
% t0  = 1342332088; % 16.1 dB asqz, 60 sec
% t0  = 1342330718; % 4.4 dB sqz, 260 sec

% phi = [NaN,    NaN,  NaN,  104,  200,  190,  180,  170,  160,  150,  140,  130,  120,  110,  100,  90,   80,   70,   60   ];
% t0 =  [130000, 5880, 4023, 6557, 7240, 7307, 7449, 7515, 7609, 7696, 7777, 7857, 7928, 8006, 8405, 8825, 8920, 9037, 9164 ];
% dur = [120,    120,  120,  300,  60,   130,  62,   84,   81,   76,   74,   64,   72,   390,  413,  89,   110,  117,  186  ];
% t0 = t0 + 1343170000;


t0 = 1342052400; dur = 1342183800 - t0;
% t0 = 1342060000; dur = 10000;

%chans = {'L1:OAF-CAL_DARM_DQ'};
% chans = {'L1:OAF-CAL_DARM_DQ','L1:GDS-CALIB_STRAIN_CLEAN'}; 
% chans = {'L1:GDS-CALIB_STRAIN_CLEAN'}; 
% chans = {'L1:OMC-DCPD_SUM_OUT_DQ'};
chans = {'L1:GRD-ISC_LOCK_STATE_N'; ...
    'L1:LSC-PRC_GAIN_MON';...
    'L1:CAM-ETMY_X'; ...
    'L1:CAM-ETMY_WX'; ...
    'L1:CAM-ETMY_Y'; ...
    'L1:CAM-ETMY_WY'; ...
    };
%oot = cdsgetdata(chans,'raw',t0,dur,'cit');

alldata = {};
deltat = 10000;
num = floor(dur/deltat);
dur = [deltat*ones(1,num), dur - deltat*(num)];
for j = 1:length(dur)
    try
        oot = get_data2(chans,'raw',t0 + sum(dur(1:j-1)), dur(j),'llo');
    catch ME
        error('nds2 error');
    end
    alldata = [alldata; oot];
    clear oot
    save 071622_long_lock.mat
end





return;

% date0=['jul 18 2022 06:30:00 UTC']; 
% chans = {'L1:OAF-CAL_DARM_DQ','L1:GDS-CALIB_STRAIN_CLEAN'}; 
% t0  = gps(date0);
% oot2 = get_data2(chans,'raw',t0,dur,'llo');

%%
oaf = oot(1).data;
% gds = oot(2).data;
% oaf2 = oot2(1).data;
% gds2 = oot2(2).data;

fs   = oot(1).rate;
nfft = 16*fs;

% [Pxx,f] = pwelch(oaf,hanning(nfft),nfft/2,nfft,fs);
% 
% cal     = load('/home/valery.frolov/.afom/oaf_cal_correction_2022-05-13.txt');
% calib   = interp1(cal(:,1),cal(:,2),f,'nearest','extrap');
% 
% darm1    = sqrt(Pxx).*10.^(calib/20);

% [Pxx,~] = pwelch(oaf2,hanning(nfft),nfft/2,nfft,fs);
% darm3    = sqrt(Pxx).*10.^(calib/20);

%tt=1/fs:1/fs:dur;
%callines= 1.35e-20*(1.334*sin(2*pi*15.1*tt-0.3915*pi)...
%                   +0.997*sin(2*pi*15.7*tt-0.8225*pi)...
%                   +1.098*sin(2*pi*16.3*tt+0.208*pi)...
%                   +1.45*sin(2*pi*16.9*tt-0.59*pi));
%[Pxx,f] = pwelch(callines,hanning(nfft),nfft/2,nfft,fs);
%clns    = sqrt(Pxx).*sqrt(1+(f/11.1e3).^2).^3;
%gdss=gds-callines';
              
[Pxx,f] = pwelch(oaf,hanning(nfft),nfft/2,nfft,fs);
darm2    = sqrt(Pxx).*sqrt(1+(f/11.1e3).^2).^3;

% [Pxx,~] = pwelch(gds2,hanning(nfft),nfft/2,nfft,fs);
% darm4    = sqrt(Pxx).*sqrt(1+(f/11.1e3).^2).^3;

% return;




%load darm15feb2021_0910utc.mat
%date0=['feb 15 2021 9:10:00 UTC']; 

% loglog(f,smooth(darm2,1),'r',f,smooth(darm4,1),'b','linewidth',1.5)
loglog(f,smooth(darm2,1))

grid on
xlim([10 5000])
ylim([3e-24 1e-20])

%legend(date,date0)
title('L1:GDS-CALIB\_STRAIN\_CLEAN')

return

%%
figure(337)
loglog(f,smooth(darm2./darm4,30))
grid on
ylim([0.6 1.5])
xlim([10 5000])

r1=InspiralRange(f,gds)/1e3;
return
%%
f2 = logspace(log10(4),log10(7000), 8089);
d2 = interp1(f, darm2, f2);

clear y
kk=1;
for ii=20:length(f2)
    y(1,kk)=f2(ii);
    y(2,kk)=d2(ii);
    kk=kk+1;
end
fid = fopen('darm21july2022n.txt','w');
fprintf(fid,'%4.3f  %e\n',y);
fclose(fid);
figure(333)
hold on
loglog(f2,d2)
hold off
%%
addpath('l1nb')
[F,R0]=InspiralRange2(f,darm2);
R1=R0/1e3;
[F,R0]=InspiralRange2(f,darm4);
R2=R0/1e3;

figure(1010)
plot(F,R2,'color',[0 0.5 0],'linewidth',2)
hold on
plot(F,R1,'linewidth',2)
hold off
xlim([10 450])
grid on

%%
%save darm29may2022_0700utc.mat f darm1 darm2

%%
%darm3=darm1; % oaf
%darm4=darm2; % gds
%save darm15feb2021_0910utc.mat f darm3 darm4